import { useMemo, useRef } from 'react';
import { TransdimensionalRectOptions } from '../../types';
import { useTransdimensionalRect } from '.';

export const useTransdimensionalRectLink = (
  options: TransdimensionalRectOptions
): {
  link: (element: Element | null) => void;
  unlink: () => void;
} => {
  const linkedRef = useRef<boolean>(false);
  const elementRef = useRef<Element | null>(null);
  const intersectionObserverElementRef = useRef<HTMLDivElement | null>(null);
  const { handleRect } = useTransdimensionalRect(options);
  // const rectRef = useRef<DOMRect | null>(null);
  const measure = (_event?: any) => {
    const element = elementRef.current;

    if (!element) return;

    measureElement(element);
  };
  const measureElement = (element: Element) => {
    const rect = element.getBoundingClientRect();

    handleRect(rect);

    // rectRef.current = rect;
  };
  const handleResize = (_event: any) => {
    console.log('WINDOW RESIZED', window.innerWidth, window.innerHeight);

    measure();
  };

  const updatingIntersectionObserverElementRef = useRef<boolean>(false);
  const updateIntersectionObserverElement = () => {
    const intersectionObserverElement = intersectionObserverElementRef.current;

    if (!intersectionObserverElement) return;

    updatingIntersectionObserverElementRef.current = true;

    const rect = intersectionObserverElement.getBoundingClientRect();

    Object.assign(intersectionObserverElement.style, {
      marginTop: `${
        parseFloat(intersectionObserverElement.style.marginTop || '0') -
        rect.top -
        1
      }px`,
      marginLeft: `${
        parseFloat(intersectionObserverElement.style.marginLeft || '0') -
        rect.left -
        1
      }px`,
    });

    updatingIntersectionObserverElementRef.current = false;
  };

  const resizeObserver = useMemo(
    () =>
      new ResizeObserver((entries) => {
        for (const entry of entries) {
          const { target } = entry;

          measureElement(target);
        }
      }),
    []
  );

  const intersectionObserver = useMemo(
    () =>
      new IntersectionObserver(
        (entries) => {
          const visiblePixels = Math.round(entries[0].intersectionRatio * 4);

          if (visiblePixels !== 1) {
            updateIntersectionObserverElement();
            measure();
          }
        },
        {
          threshold: [0.125, 0.375, 0.625, 0.875],
        }
      ),
    []
  );

  const link = useMemo(
    () => (element: Element | null) => {
      if (!linkedRef.current) {
        linkedRef.current = true;

        window.addEventListener('resize', handleResize, {
          passive: true,
        });
      }

      if (element === elementRef.current) return;

      if (elementRef.current) {
        // todo remove intersection observer element

        resizeObserver.unobserve(elementRef.current);
      }

      elementRef.current = element;

      if (element) {
        if (!intersectionObserverElementRef.current) {
          const intersectionObserverElement =
            (intersectionObserverElementRef.current =
              document.createElement('div'));

          Object.assign(intersectionObserverElement.style, {
            position: 'fixed',
            // display: 'none',
            pointerEvents: 'none',
            width: '2px',
            height: '2px',
            // background: 'red',
          });

          element.prepend(intersectionObserverElement);

          updateIntersectionObserverElement();

          intersectionObserver.observe(intersectionObserverElement);
        }

        setTimeout(() => {
          resizeObserver.observe(element);
        }, 0);
      }
    },
    [handleRect]
  );

  const unlink = useMemo(
    () => () => {
      linkedRef.current = false;

      if (intersectionObserverElementRef.current) {
        intersectionObserverElementRef.current.parentNode?.removeChild(
          intersectionObserverElementRef.current
        );
      }

      window.removeEventListener('resize', handleResize);
      resizeObserver.disconnect();
      intersectionObserver.disconnect();

      handleRect(null);
    },
    [handleRect]
  );

  return {
    link,
    unlink,
  };
};

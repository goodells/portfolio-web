import { useEffect, useRef } from 'react';

import { minArray, maxArray } from '../../utilities';
import { useLayoutPoint } from '.';

export const useLayoutPointEffect = ({
  layoutPoint,
  timeout,
  onTrigger,
}: {
  layoutPoint: string;
  timeout?: number;
  onTrigger?: (event?: Event) => void;
}): void => {
  const layoutPointScreenY = useLayoutPoint(layoutPoint);
  const previousScrollYRef = useRef(window.scrollY);

  useEffect(() => {
    if (layoutPointScreenY === null) return;

    const handleScroll = (event: Event) => {
      const deltaScrollYs = [window.scrollY, previousScrollYRef.current];
      const lesserScrollY = minArray(deltaScrollYs);
      const greaterScrollY = maxArray(deltaScrollYs);

      if (
        lesserScrollY <= layoutPointScreenY &&
        greaterScrollY >= layoutPointScreenY
      ) {
        if (onTrigger) onTrigger(event);
      }

      previousScrollYRef.current = window.scrollY;
    };

    window.addEventListener('scroll', handleScroll, { passive: true });

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [layoutPointScreenY, timeout, onTrigger]);
};

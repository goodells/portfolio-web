import { useState, useEffect } from 'react';
import { Vector2 } from 'three';
import { getWindowSizeVector2 } from '../../utilities';

export const useWindowSize = (): Vector2 => {
  const [windowSize, setWindowSize] = useState(getWindowSizeVector2());

  useEffect(() => {
    const handleResize = () => {
      setWindowSize(getWindowSizeVector2());
    };

    window.addEventListener('resize', handleResize, { passive: true });

    handleResize();

    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return windowSize;
};

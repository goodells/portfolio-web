import { useContext } from 'react';
import { EnvironmentContext } from '../../contexts';

export const useEnvironment = () => {
  return useContext(EnvironmentContext);
};

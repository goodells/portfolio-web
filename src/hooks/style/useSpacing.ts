import { useTheme } from '@mui/material';

export const useSpacing = (spacing: number = 1) => {
  const theme = useTheme();

  return Number(theme.spacing(spacing).slice(0, -2));
};

import { lighten, darken, useTheme } from '@mui/material';

export const useEnhance = () => {
  const theme = useTheme();

  switch (theme.palette.mode) {
    case 'dark':
      return lighten;
    case 'light':
      return darken;
    default:
      return (color: string, _amount: number) => color;
  }
};

import { debugSlice } from '../slices';

export const { setDebugMode } = debugSlice.actions;

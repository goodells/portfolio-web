import { layoutSlice } from '../slices';

export const { setLayoutAnchorScreenPosition } = layoutSlice.actions;

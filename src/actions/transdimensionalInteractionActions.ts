import { transdimensionalInteractionSlice } from '../slices';

export const {
  setTransdimensionalRect,
  setTransdimensionalGridContainerConfiguration,
  setTransdimensionalGridItemSize,
} = transdimensionalInteractionSlice.actions;

import { createAction } from '@reduxjs/toolkit';
import { threeFontsSlice } from '../slices';

export const { setThreeFont } = threeFontsSlice.actions;

export const requestThreeFont = createAction<{
  id: string;
  url: string;
}>('threeFonts/requestThreeFont');

/// <reference types="vite/client" />

import { FrameloopMode } from './types';

interface ImportMetaEnv {
  readonly VITE_CALENDLY_PROXY_URL: string;
  readonly VITE_FRAMELOOP_MODE: FrameloopMode;
}

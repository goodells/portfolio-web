import { createContext } from 'react';
import { Environment } from '../types';
import { getEnvironment } from '../utilities';

const EnvironmentContext = createContext<Environment>(getEnvironment());
EnvironmentContext.displayName = 'EnvironmentContext';

export { EnvironmentContext };

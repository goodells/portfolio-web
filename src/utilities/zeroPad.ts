export const zeroPad = (value: number, places: number) => {
  const numberOfZeros = places - value.toString().length + 1;

  return Array(+(numberOfZeros > 0 && numberOfZeros)).join('0') + value;
};

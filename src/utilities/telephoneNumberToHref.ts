export const telephoneNumberToHref = (telephoneNumber: string) =>
  'tel:' + telephoneNumber.replace(/\(\)\s\t-/gi, '');

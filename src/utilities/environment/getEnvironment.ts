import { Environment } from '../../types';
import {
  DEFAULT_CALENDLY_PROXY_URL,
  DEFAULT_FRAMELOOP_MODE,
} from '../../constants';
import { CalendlyProxyClient } from '../../clients';
import { getLocaleTimeZoneName } from '..';

export const getEnvironment = (): Environment => {
  const calendlyProxyUrl =
    import.meta.env.VITE_CALENDLY_PROXY_URL ?? DEFAULT_CALENDLY_PROXY_URL;
  const frameloopMode =
    import.meta.env.VITE_FRAMELOOP_MODE ?? DEFAULT_FRAMELOOP_MODE;

  return {
    isDevelopment: import.meta.env.DEV,
    isProduction: import.meta.env.PROD,
    calendlyProxyClient: new CalendlyProxyClient(calendlyProxyUrl),
    calendlyProxyUrl,
    frameloopMode,
    localeTimeZoneName: getLocaleTimeZoneName(),
  };
};

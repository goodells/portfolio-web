export const getHashValue = (hash: string | null | undefined) =>
  (hash || '#').replace(/^#/, '') || null;

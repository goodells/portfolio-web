export const getLocaleTimeZoneName = () => {
  return new Date()
    .toLocaleDateString(undefined, { day: '2-digit', timeZoneName: 'long' })
    .substring(4);
};

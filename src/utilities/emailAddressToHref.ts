export const emailAddressToHref = (emailAddress: string) =>
  'mailto:' + emailAddress;

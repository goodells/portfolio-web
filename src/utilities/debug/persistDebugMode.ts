// Constants
import { PERSISTENCE_KEY_DEBUG_MODE } from '../../constants';

export const persistDebugMode = (value: boolean) => {
  localStorage.setItem(PERSISTENCE_KEY_DEBUG_MODE, JSON.stringify(value));
};

import { Vector2 } from 'three';

import { pointToString } from '.';

export const pointsToString = (points: Vector2[]): string => {
  return points.map(pointToString).join(' ');
};

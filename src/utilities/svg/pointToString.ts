import { Vector2 } from 'three';

import { roundScalar } from '..';

export const pointToString = (point: Vector2): string => {
  return `${roundScalar(point.x)},${roundScalar(point.y)}`;
};

export const roundScalar = (value: number) => Math.round(value * 2) / 2;

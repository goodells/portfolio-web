export const staggerMany = ({
  value,
  count,
  step,
}: {
  value: number;
  count: number;
  step: number;
}): number[] => {
  const padding = (count - 1) * step;
  const range = 1 + padding;
  const values: number[] = [];

  for (let index = 0; index < count; index++) {
    const ratio = (-padding + index * step + value * (range + padding)) / range;

    values.push(ratio);
  }

  return values;
};

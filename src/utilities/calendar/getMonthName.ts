import { MONTH_NAMES } from '../../constants';

export const getMonthName = (monthIndex: number): string => {
  return MONTH_NAMES[monthIndex];
};

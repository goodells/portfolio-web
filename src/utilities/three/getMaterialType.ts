import {
  Material,
  MeshBasicMaterial,
  MeshLambertMaterial,
  MeshPhongMaterial,
  MeshStandardMaterial,
} from 'three';

import { MaterialType } from '../../types';

export const getMaterialType = (material: Material | Material[]) => {
  if (Array.isArray(material)) return null;

  switch (true) {
    case material instanceof MeshBasicMaterial:
      return MaterialType.Basic;
    case material instanceof MeshStandardMaterial:
      return MaterialType.Standard;
    case material instanceof MeshPhongMaterial:
      return MaterialType.Phong;
    case material instanceof MeshLambertMaterial:
      return MaterialType.Lambert;
    default:
      return null;
  }
};

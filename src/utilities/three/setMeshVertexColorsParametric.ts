import { Box3, BufferAttribute, Color, Mesh, Object3D, Vector3 } from 'three';

import { BoxColorCoercible, BoxCornerColors } from '../../types';
import { toBoxCornerColors } from '.';

const COLOR_ITEM_SIZE = 3;

const getParametricBoxColor = (
  boxColors: BoxCornerColors,
  parameter: Vector3
): Color => {
  return boxColors.backBottomLeft
    .clone()
    .lerp(boxColors.backBottomRight, parameter.x)
    .lerp(
      boxColors.backTopLeft.clone().lerp(boxColors.backTopRight, parameter.x),
      parameter.y
    )
    .lerp(
      boxColors.frontBottomLeft
        .clone()
        .lerp(boxColors.frontBottomRight, parameter.x)
        .lerp(
          boxColors.frontTopLeft
            .clone()
            .lerp(boxColors.frontTopRight, parameter.x),
          parameter.y
        ),
      parameter.z
    );
};

export const setMeshVertexColorsParametric = (
  parent: Object3D,
  meshColor: BoxColorCoercible,
  box: Box3 = new Box3().setFromObject(parent)
) => {
  const boxColors = toBoxCornerColors(meshColor);

  parent.traverse((object) => {
    if (object.type !== 'Mesh') return;

    const mesh = object as Mesh;
    mesh.updateMatrix();
    mesh.updateMatrixWorld(true);

    const positionAttribute = mesh.geometry.getAttribute('position');
    const colorAttribute = new BufferAttribute(
      new Float32Array(positionAttribute.count * COLOR_ITEM_SIZE),
      COLOR_ITEM_SIZE
    );

    for (
      let vertexIndex = 0;
      vertexIndex < positionAttribute.count;
      vertexIndex++
    ) {
      const vertexRelativeToMesh = new Vector3().fromBufferAttribute(
        positionAttribute,
        vertexIndex
      );
      const vertexRelativeToWorld = mesh.localToWorld(vertexRelativeToMesh);
      const parameter = box.getParameter(vertexRelativeToWorld, new Vector3());
      const vertexColor = getParametricBoxColor(boxColors, parameter);

      colorAttribute.setXYZ(
        vertexIndex,
        vertexColor.r,
        vertexColor.g,
        vertexColor.b
        // parameter.x,
        // parameter.y,
        // parameter.z
      );
    }

    mesh.geometry.setAttribute('color', colorAttribute);

    if (!Array.isArray(mesh.material)) {
      mesh.material.vertexColors = true;
      mesh.material.needsUpdate = true;
    }
  });
};

import { Color, Vector3 } from 'three';

import { BoxCornerColors } from '../../types';

export const getParametricBoxColors = (
  _boxColors: BoxCornerColors,
  parameter: Vector3
) => {
  // @todo
  return new Color(parameter.x, parameter.y, parameter.z);
};

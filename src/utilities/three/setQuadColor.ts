import { Color, Mesh, MeshBasicMaterial, MeshPhongMaterial } from 'three';

import { PointColorCoercible } from '../../types';
import { toColor } from '.';

export const setQuadColor = (mesh: Mesh, meshColor: PointColorCoercible) => {
  const { geometry, material } = mesh;

  if (!meshColor) {
    meshColor = new Color(1, 1, 1);
  }

  if (
    typeof meshColor === 'string' &&
    meshColor.startsWith('linear-gradient')
  ) {
    console.log({ geometry });
  } else if (!Array.isArray(material) && meshColor instanceof Color) {
    if (
      material instanceof MeshBasicMaterial ||
      material instanceof MeshPhongMaterial
    ) {
      material.vertexColors = false;
      material.color = toColor(meshColor);
      material.needsUpdate = true;
    }
  }
};

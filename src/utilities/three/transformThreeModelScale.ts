import { Vector3 } from 'three';

import { Vector3Coercible } from '../../types';

import { toVector3 } from '.';

export const transformThreeModelScale = (scale: Vector3Coercible): Vector3 =>
  toVector3(scale).multiplyScalar(1 / 2);

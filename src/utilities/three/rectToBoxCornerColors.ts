import { BoxCornerColors, RectCornerColors } from '../../types';
import { getBoxCornerColorsFromFrontAndBackRects } from '.';

export const rectToBoxCornerColors = (
  rect: RectCornerColors
): BoxCornerColors => getBoxCornerColorsFromFrontAndBackRects(rect, rect);

import { Object3D } from 'three';

import { standardMaterial } from '../../materials';
import { applyMaterial } from '.';

export const applyStandardMaterial = (modelObject: Object3D) =>
  applyMaterial(modelObject, standardMaterial);

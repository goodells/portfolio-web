import { Object3D, Vector3 } from 'three';

export const getAncestorScale = (parent: Object3D | null): Vector3 => {
  if (!parent) {
    return new Vector3(1, 1, 1);
  }

  return parent.scale.clone().multiply(getAncestorScale(parent.parent));
};

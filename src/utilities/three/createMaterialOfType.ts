import {
  Color,
  MeshBasicMaterial,
  MeshLambertMaterial,
  MeshPhongMaterial,
  MeshStandardMaterial,
} from 'three';

import { MaterialType } from '../../types';

const DEFAULT_OPTIONS = {
  vertexColors: true,
};

export const createMaterialOfType = (
  materialType: MaterialType,
  color?: Color
) => {
  switch (materialType) {
    case MaterialType.Basic:
      return new MeshBasicMaterial({
        ...DEFAULT_OPTIONS,
        color,
      });
    case MaterialType.Standard:
      return new MeshStandardMaterial({
        ...DEFAULT_OPTIONS,
        color,
      });
    case MaterialType.Phong:
      return new MeshPhongMaterial({
        ...DEFAULT_OPTIONS,
        color,
      });
    case MaterialType.Lambert:
      return new MeshLambertMaterial({
        ...DEFAULT_OPTIONS,
        color,
      });
  }
};

import { LAYOUT_POINT_VARIABLE_SUFFIX_Y } from '../../constants';

export const toLayoutPointY = (anchorId: string) =>
  anchorId + LAYOUT_POINT_VARIABLE_SUFFIX_Y;

import { Vector2 } from 'three';

export const getWindowSizeVector2 = () =>
  new Vector2(window.innerWidth, window.innerHeight);

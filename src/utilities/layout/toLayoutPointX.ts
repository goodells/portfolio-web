import { LAYOUT_POINT_VARIABLE_SUFFIX_X } from '../../constants';

export const toLayoutPointX = (anchorId: string) =>
  anchorId + LAYOUT_POINT_VARIABLE_SUFFIX_X;

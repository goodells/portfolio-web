import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { THREE_MODELS, THREE_FONTS } from './constants';
import {
  debugSlice,
  layoutSlice,
  threeModelsSlice,
  threeFontsSlice,
  transdimensionalInteractionSlice,
} from './slices';
import { debugSaga, threeModelsSaga, threeFontsSaga } from './sagas';
import { requestThreeFont, requestThreeModel } from './actions';

const sagaMiddleware = createSagaMiddleware();
const store = configureStore({
  reducer: {
    debug: debugSlice.reducer,
    layout: layoutSlice.reducer,
    threeModels: threeModelsSlice.reducer,
    threeFonts: threeFontsSlice.reducer,
    transdimensionalInteraction: transdimensionalInteractionSlice.reducer,
  },
  middleware: [sagaMiddleware],
});

sagaMiddleware.run(debugSaga);
sagaMiddleware.run(threeModelsSaga);
sagaMiddleware.run(threeFontsSaga);

export type RootState = ReturnType<typeof store.getState>;

export default store;

setTimeout(() => {
  Array.from(THREE_MODELS.entries()).forEach((entry) => {
    const [id, { url, transform }] = entry;
    const action = requestThreeModel({ id, url, transform });

    store.dispatch(action);
  });

  Array.from(THREE_FONTS.entries()).forEach((entry) => {
    const [id, { url }] = entry;
    const action = requestThreeFont({ id, url });

    store.dispatch(action);
  });
}, 0);

import { Project } from '../../types';
import {
  PROJECT_HOMEBRIDGE_BRAND_COLOR_PRIMARY,
  PROJECT_HOMEBRIDGE_BRAND_COLOR_SECONDARY,
} from './homebridge';

export const PROJECT_HOMEBRIDGE_NANOLEAF_MULTI: Project = {
  id: 'homebridge-nanoleaf-multi',
  name: 'Homebridge Nanoleaf Multi',
  description:
    'Homebridge plugin for individual color control of Nanoleaf light effects',
  status: 'maintenance',
  chip: {
    background: `linear-gradient(135deg, ${PROJECT_HOMEBRIDGE_BRAND_COLOR_PRIMARY} 0%, ${PROJECT_HOMEBRIDGE_BRAND_COLOR_SECONDARY} 100%)`,
    color: 'rgb(255, 255, 255)',
    content: 'Homebridge Nanoleaf Multi',
  },
  page: {
    tabs: [
      {
        label: 'Overview',
        page: {
          markdown: `\
_TODO_
`,
        },
      },
    ],
  },
};

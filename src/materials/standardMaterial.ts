import { MeshPhongMaterial } from 'three';

export const standardMaterial = new MeshPhongMaterial({
  color: 0x808080,
  flatShading: true,
});

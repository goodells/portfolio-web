import { MeshStandardMaterial } from 'three';

export const standardAccentMaterial = new MeshStandardMaterial({
  color: 0x707070,
});

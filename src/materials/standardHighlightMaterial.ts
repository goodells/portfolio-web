import { MeshStandardMaterial } from 'three';

export const standardHighlightMaterial = new MeshStandardMaterial({
  color: 0xff00ff,
});

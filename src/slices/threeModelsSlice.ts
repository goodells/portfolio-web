import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Object3D } from 'three';

export type ThreeModelsState = {
  threeModelsById: {
    [modelId: string]: Object3D;
  };
};

const INITIAL_STATE: ThreeModelsState = {
  threeModelsById: {},
};

export const threeModelsSlice = createSlice({
  name: 'threeModels',
  initialState: INITIAL_STATE,
  reducers: {
    setThreeModel: (
      state,
      action: PayloadAction<{
        id: string;
        model: Object3D;
      }>
    ) => {
      const { id, model } = action.payload;

      state.threeModelsById = {
        ...state.threeModelsById,
        [id]: model as any,
      };
    },
  },
});

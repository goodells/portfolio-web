import { FC } from 'react';
import { Helmet } from 'react-helmet';

export const PageTitle: FC<{ children: string }> = ({ children }) => {
  return (
    <Helmet titleTemplate="%s | Samuel Goodell">
      <title>{children}</title>
    </Helmet>
  );
};

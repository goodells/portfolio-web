import { PropsWithChildren, forwardRef } from 'react';
import { DialogBaseProps, DialogBase } from './DialogBase';
import { SxProps, Theme } from '@mui/material';

export interface DynamicDialogProps extends DialogBaseProps {
  sx?: SxProps<Theme>;
}

export const DynamicDialog = forwardRef<
  HTMLDivElement,
  PropsWithChildren<DynamicDialogProps>
>(({ sx, children, ...baseProps }, ref) => {
  return (
    <DialogBase ref={ref} {...baseProps} sx={sx}>
      {children}
    </DialogBase>
  );
});

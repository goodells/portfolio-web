import { FC, PropsWithChildren } from 'react';

import './ApplicationLoadingContainer.scss';

export const ApplicationLoadingContainer: FC<PropsWithChildren<{}>> = ({
  children,
}) => {
  return <>{children}</>;
};

import {
  FC,
  PropsWithChildren,
  MutableRefObject,
  useEffect,
  useRef,
} from 'react';
import { Group, Mesh, Object3D } from 'three';
import { useSelector } from 'react-redux';
import { getOrCreateThreeModelSelector } from '../../selectors';
import {
  setMeshVertexColorsParametric,
  transformThreeModelScale,
} from '../../utilities';
import { ObjectProps, MeshProps } from '../../types';
import { useThree } from '@react-three/fiber';

export interface ThreeModelProps extends ObjectProps, MeshProps {
  id: string;
  log?: boolean;
}

export const ThreeModel: FC<PropsWithChildren<ThreeModelProps>> = ({
  id,
  position,
  scale,
  rotation,
  log,
  ...props
}) => {
  const { invalidate } = useThree();
  const groupRef = useRef<Group>();
  const modelRef = useRef<Object3D>();
  const threeModelOriginal = useSelector(getOrCreateThreeModelSelector(id));

  useEffect(() => {
    (async () => {
      const group = groupRef.current;

      if (!group) {
        if (log) console.log('group ref not set');

        return;
      }

      if (!threeModelOriginal) {
        if (log) console.log('model not loaded');

        return;
      }

      if (log) console.log('adding model to group', threeModelOriginal);

      const model = (modelRef.current = threeModelOriginal.clone(true));

      group.add(model);
      group.traverse((object) => {
        if (object.type !== 'Mesh') return;

        const mesh = object as Mesh;

        if (typeof props.castShadow === 'boolean') {
          mesh.castShadow = props.castShadow;
        }

        if (typeof props.receiveShadow === 'boolean') {
          mesh.receiveShadow = props.receiveShadow;
        }

        if (props.material) {
          mesh.material = props.material;
        }
      });

      setTimeout(() => {
        if (props.vertexColors && props.color) {
          group.updateMatrix();
          group.updateMatrixWorld(true);

          setMeshVertexColorsParametric(group, props.color);

          invalidate();
        }
      }, 0);
    })();
  }, [threeModelOriginal]);

  useEffect(() => {
    const group = groupRef.current;

    if (!group) {
      return;
    }

    group.traverse((object) => {
      if (object.type !== 'Mesh') return;

      const mesh = object as Mesh;

      if (props.material) {
        mesh.material = props.material;
      }
    });
  }, [props.material]);

  if (log) {
    console.log('render with position', position);
  }

  return (
    <group
      ref={groupRef as MutableRefObject<any>}
      scale={scale && transformThreeModelScale(scale)}
      position={position}
      rotation={rotation}
    />
  );
};

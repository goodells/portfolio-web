import { FC } from 'react';

export const ThreeLighting: FC = () => {
  return (
    <>
      <ambientLight intensity={2.5} />
      <directionalLight color="white" position={[10, 5, 7.5]} intensity={1} />
    </>
  );
};

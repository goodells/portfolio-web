import React, { FC, MutableRefObject, useRef } from 'react';
import { Group, Object3D } from 'three';

export const ThreeInterceptor: FC<{
  children: (parent: Object3D) => React.ReactChild;
}> = ({ children }) => {
  const ref = useRef<Group>();

  return (
    <group ref={ref as MutableRefObject<any>}>
      {ref.current && children(ref.current)}
    </group>
  );
};

// import { FC, useState, useEffect, useContext, useRef, useMemo } from 'react';
// import { Material, MeshBasicMaterial, MeshStandardMaterial } from 'three';
// import {
//   TransdimensionalUnitConversionServiceContext,
//   TransdimensionalInteractionServiceContext,
// } from '../../contexts';
// import { BoxColorCoercible, ElementState } from '../../types';
// import { ThreeBox, ThreeTransdimensionalRect } from '.';
// import { useTheme } from '@mui/material';
// import { useMaterial } from '../../hooks';
// import { enhance, toColor } from '../../utilities';

// export const ThreeTransdimensionalButton: FC<{
//   id: string;
//   color?: BoxColorCoercible;
//   baseMaterial?: Material;
//   depth?: number;
// }> = ({ id, baseMaterial, ...props }) => {
//   const theme = useTheme();
//   const transdimensionalUnitConversionService = useContext(
//     TransdimensionalUnitConversionServiceContext
//   );
//   const transdimensionalInteractionService = useContext(
//     TransdimensionalInteractionServiceContext
//   );

//   const depth = props.depth || theme.depth.button;
//   // const variant = props.variant || "default";

//   const initialColor = toColor(theme.palette.background.paper);
//   const hoverColor = toColor(enhance(theme.palette.background.paper, 0.1));

//   const materialRef = useRef<MeshStandardMaterial | MeshBasicMaterial>(
//     new MeshStandardMaterial()
//   );
//   const material = materialRef.current;
//   const [state, setState] = useState<ElementState | null>(null);

//   useEffect(() => {
//     transdimensionalInteractionService.onChange(id, setState as any);
//   }, [transdimensionalInteractionService, setState]);

//   useEffect(() => {
//     if (!state) return;
//   }, [state]);

//   return (
//     <ThreeTransdimensionalRect id={id}>
//       <ThreeBox
//         position={[0, 0, -depth / 2]}
//         scale={[1, 1, depth]}
//         color={state?.boxColor ?? undefined}
//         material={material}
//       />
//     </ThreeTransdimensionalRect>
//     // <ThreeBox
//     //   scale={[
//     //     transdimensionalUnitConversionService.clientToThreeX(state.width),
//     //     transdimensionalUnitConversionService.clientToThreeY(state.height),
//     //     depth,
//     //   ]}
//     //   position={[
//     //     -transdimensionalUnitConversionService.clientToThreeX(
//     //       transdimensionalUnitConversionService.getContainerWidth() / 2 -
//     //         state.x -
//     //         state.width / 2
//     //     ),
//     //     -transdimensionalUnitConversionService.clientToThreeY(
//     //       state.y + state.height / 2
//     //     ),
//     //     -depth / 2,
//     //   ]}
//     //   material={material}
//     // />
//   );
// };

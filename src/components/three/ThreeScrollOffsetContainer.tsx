import {
  FC,
  PropsWithChildren,
  MutableRefObject,
  useRef,
  useEffect,
  useContext,
} from 'react';
import { Group, Vector2 } from 'three';
import { RootState, useFrame, useThree } from '@react-three/fiber';
import { TransdimensionalUnitConversionServiceContext } from '../../contexts';
import { useWindowSize } from '../../hooks';

export const ThreeScrollOffsetContainer: FC<
  PropsWithChildren<{
    cameraFractionalOffset?: Vector2;
    sceneFractionalOffset?: Vector2;
    frameloop?: boolean;
  }>
> = ({ children, ...props }) => {
  const transdimensionalUnitConversionService = useContext(
    TransdimensionalUnitConversionServiceContext
  );
  const state = useThree();
  const windowSize = useWindowSize();
  const groupRef = useRef<Group>();

  const update = (state: RootState) => {
    if (!transdimensionalUnitConversionService) return;
    if (!groupRef.current) return;

    const sceneCenter =
      transdimensionalUnitConversionService.getWindowCenterThree();
    const relativeScroll =
      transdimensionalUnitConversionService.getRelativeScroll();
    const visibleSize =
      transdimensionalUnitConversionService.getVisibleSizeAtDepth(0);
    const rendererSize =
      transdimensionalUnitConversionService.getRendererSize();

    groupRef.current.position.x = props.sceneFractionalOffset
      ? props.sceneFractionalOffset.x * visibleSize.x
      : 0;
    groupRef.current.position.y = visibleSize.y / 2;

    state.camera.setViewOffset(
      rendererSize.x,
      rendererSize.y,
      -relativeScroll.x,
      -relativeScroll.y,
      rendererSize.x,
      rendererSize.y
    );
    state.camera.position.x =
      sceneCenter.x +
      (props.cameraFractionalOffset
        ? props.cameraFractionalOffset.x * visibleSize.x
        : 0);
    state.camera.position.y = sceneCenter.y;

    state.invalidate();
  };

  if (props.frameloop) {
    useFrame((state) => update(state));
  }

  useEffect(() => {
    if (!transdimensionalUnitConversionService) return;

    const handleScroll = () => {
      update(state);
    };

    handleScroll();

    window.addEventListener('scroll', handleScroll, { passive: true });

    return () => window.removeEventListener('scroll', handleScroll);
  }, [state, groupRef.current, windowSize.x, windowSize.y]);

  return <group ref={groupRef as MutableRefObject<any>}>{children}</group>;
};

import {
  FC,
  PropsWithChildren,
  ReactNode,
  useRef,
  useEffect,
  useLayoutEffect,
  useMemo,
} from 'react';
import { createMemoryHistory } from 'history';
import { useInView } from 'react-intersection-observer';
import classNames from 'classnames';
import { Provider as ReduxProvider, useSelector } from 'react-redux';
import * as THREE from 'three';
import { Vector2, Fog, PerspectiveCamera } from 'three';
import { RootState as ThreeRootState, Canvas } from '@react-three/fiber';
import { useContextBridge } from '@react-three/drei';
import Stats from 'stats.js';
import { SxProps, Theme, ThemeProvider, useTheme } from '@mui/material';
import {
  THREE_CAMERA_DEPTH,
  THREE_DEFAULT_FOV,
  THREE_DEFAULT_NEAR,
  THREE_DEFAULT_FAR,
  THREE_DEFAULT_FOG_NEAR,
  THREE_DEFAULT_FOG_FAR,
} from '../../constants';
import store from '../../store';
import { useWindowSize } from '../../hooks';
import { getOrCreateTransdimensionalRectSelector } from '../../selectors';
import { toColor } from '../../utilities';
import { TransdimensionalUnitConversionService } from '../../services';
import {
  TransdimensionalUnitConversionServiceContext,
  TransdimensionalInteractionServiceContext,
  ThreeStatisticsContext,
} from '../../contexts';
import {
  DebugThreeStatisticsUpdater,
  DebugThreeStatistics,
  TransdimensionalRect,
} from '..';

import './Three.scss';

export interface ThreeLayer {
  zIndex: number;
  near: number;
  far: number;
}

const DEFAULT_LAYERS: ThreeLayer[] = [
  {
    zIndex: 0,
    near: THREE_DEFAULT_NEAR,
    far: THREE_DEFAULT_FAR,
  },
];

export const Three: FC<
  PropsWithChildren<{
    id: string;
    order?: number;
    pointerEvents?: boolean;
    frameloop?: 'always' | 'demand' | 'never';
    layers?: ThreeLayer[];
    near?: number;
    far?: number;
    fogNear?: number;
    fogFar?: number;
    inset?: boolean;
    interactive?: boolean;
    className?: string;
    sx?: SxProps<Theme>;
    fov?: number;
    fovScaling?: boolean;
    domChildren?: ReactNode;
  }>
> = ({
  id,
  order,
  pointerEvents,
  frameloop,
  near,
  far,
  fogNear,
  fogFar,
  inset,
  interactive,
  className,
  sx,
  children,
  ...props
}) => {
  const theme = useTheme();
  const windowSize = useWindowSize();
  const memoryHistory = useMemo(() => createMemoryHistory(), []);

  useLayoutEffect(() => {
    const handleLocationChange = () => {
      memoryHistory.replace(window.location);
    };

    window.addEventListener('locationchange', handleLocationChange);

    return () =>
      window.removeEventListener('locationchange', handleLocationChange);
  }, [memoryHistory]);

  // const { theme } = context;
  // const visibleHeight = transdimensionalUnitConversionService.getVisibleHeightAtDepth(0);

  const contextsRef = useRef(new Set<ThreeRootState>());

  const updateFogColor = () => {
    const fogColor = toColor(theme.palette.background.clear);

    contextsRef.current.forEach((context) => {
      context.scene.fog = new Fog(
        fogColor,
        fogNear ?? THREE_DEFAULT_FOG_NEAR,
        fogFar ?? THREE_DEFAULT_FOG_FAR
      );
    });
  };

  useEffect(() => {
    updateFogColor();
  }, [theme.palette.background.clear]);

  // const canvasHeight = roundScalar(height || 0);
  // const fovCoefficient = height ? canvasHeight / height : originalFov;
  // const canvasWidth = roundScalar((width || 0) * fovCoefficient);
  // const scaledFov = TransdimensionalUnitConversionService.multiplyFieldOfView(
  //   originalFov,
  //   fovCoefficient
  // );

  const ContextBridge = useContextBridge(
    TransdimensionalInteractionServiceContext,
    ThreeStatisticsContext
  );

  const statisticsRef = useRef(new Stats());
  const statistics = statisticsRef.current;

  const { ref } = useInView({
    threshold: 0,
    rootMargin: `${windowSize.y / 4}px`,
  });

  const handleResize = () => {
    contextsRef.current.forEach((context) => {
      // context.gl.setSize(canvasWidth, canvasHeight);
      (context.camera as PerspectiveCamera).fov = scaledFov;
    });

    // if (canvasRef.current) {
    //   const canvas = canvasRef.current;
    //   canvas.style.width = canvasWidth + 'px';
    //   canvas.style.height = canvasHeight + 'px';
    // }
  };

  const rect = useSelector(getOrCreateTransdimensionalRectSelector(id));
  const canvasHeight = rect?.height ?? 0;
  const canvasWidth = rect?.width ?? 0;
  const heightRatio = canvasHeight / windowSize.y;
  let scaledFov = props.fov || THREE_DEFAULT_FOV;

  if (props.fovScaling !== false) {
    scaledFov = TransdimensionalUnitConversionService.multiplyFieldOfView(
      scaledFov,
      heightRatio
    );
  }

  const shouldRender = canvasWidth && canvasHeight;
  const transdimensionalUnitConversionService = useMemo(
    () =>
      shouldRender
        ? new TransdimensionalUnitConversionService(
            rect,
            windowSize,
            // new Vector2(width, height),
            new Vector2(canvasWidth, canvasHeight),
            new Vector2(canvasWidth, canvasHeight),
            scaledFov,
            1,
            THREE_CAMERA_DEPTH
          )
        : null,
    [rect, windowSize, canvasWidth, canvasHeight, scaledFov]
  );

  const layers = props.layers ?? DEFAULT_LAYERS;

  useEffect(() => {
    handleResize();
  }, [rect, canvasWidth, canvasHeight, windowSize.x, windowSize.y, scaledFov]);

  return (
    <TransdimensionalRect
      id={id}
      ref={ref}
      component="div"
      className={classNames('ThreeContainer', className, {
        'ThreeContainer-inset': inset,
      })}
      sx={[
        {
          position: 'relative',
          pointerEvents: interactive ? 'initial' : 'none',
        },
        ...(Array.isArray(sx) ? sx : [sx]),
      ]}
    >
      {!!shouldRender && (
        <ThreeStatisticsContext.Provider value={statistics}>
          {layers.map((layer) => (
            <Canvas
              key={layer.zIndex}
              // ref={canvasRef}
              // frameloop={frameloop ?? (inView ? 'always' : 'demand')}
              // frameloop={environment.frameloopMode}
              frameloop="demand"
              className={classNames('Three', {
                'Three-pointerEvents': !!pointerEvents,
              })}
              onCreated={(context) => {
                // context.gl.toneMapping = THREE.NoToneMapping;
                // context.gl.outputEncoding = THREE.sRGBEncoding;
                // context.gl.shadowMap.enabled = true;

                contextsRef.current.add(context);

                updateFogColor();

                context.gl.domElement.style.opacity = '1';

                handleResize();
              }}
              gl={{
                logarithmicDepthBuffer: true,
                toneMapping: THREE.NoToneMapping,
                // outputEncoding: THREE.sRGBEncoding,
                outputColorSpace: THREE.SRGBColorSpace,
              }}
              camera={{
                fov: scaledFov,
                position: [0, 0, THREE_CAMERA_DEPTH],
                rotation: [0, 0, 0],
                near: layer.near,
                far: layer.far,
              }}
              style={{
                position: 'absolute',
                zIndex: layer.zIndex,
                overflow: inset ? 'hidden' : 'visible',
                width: canvasWidth + 'px',
                height: canvasHeight + 'px',
                pointerEvents: 'inherit',
              }}
            >
              <ContextBridge>
                <ThemeProvider theme={theme}>
                  <ReduxProvider store={store}>
                    <ThreeStatisticsContext.Provider value={statistics}>
                      <TransdimensionalUnitConversionServiceContext.Provider
                        value={transdimensionalUnitConversionService}
                      >
                        <DebugThreeStatisticsUpdater />
                        {children}
                      </TransdimensionalUnitConversionServiceContext.Provider>
                    </ThreeStatisticsContext.Provider>
                  </ReduxProvider>
                </ThemeProvider>
              </ContextBridge>
            </Canvas>
          ))}
          {props.domChildren}
          <DebugThreeStatistics />
        </ThreeStatisticsContext.Provider>
      )}
    </TransdimensionalRect>
  );
};

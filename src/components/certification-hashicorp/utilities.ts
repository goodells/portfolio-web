export const getCertificationHashicorpHeaderRectId = (id: string) =>
  `${id}-header`;

export const getCertificationHashicorpBodyRectId = (id: string) => `${id}-body`;

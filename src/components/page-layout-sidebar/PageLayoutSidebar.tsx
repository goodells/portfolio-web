import { FC, ReactNode } from 'react';
import { Box } from '@mui/material';

export const PageLayoutSidebar: FC<{
  sidebarChildren: ReactNode;
  contentChildren: ReactNode;
}> = (props) => {
  return (
    <Box component="div" sx={{ display: 'flex' }}>
      <Box component="div" sx={{ width: 256, flexShrink: 0 }}>
        {props.sidebarChildren}
      </Box>
      <Box component="div" sx={{ flexGrow: 1 }}>
        {props.contentChildren}
      </Box>
    </Box>
  );
};

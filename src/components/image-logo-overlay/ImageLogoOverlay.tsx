import { FC } from 'react';

import './ImageLogoOverlay.scss';

export const ImageLogoOverlay: FC<{
  imageUrl: string;
  logoUrl: string;
  logoWidth: number;
  logoHeight: number;
}> = ({ imageUrl, logoUrl, logoWidth, logoHeight }) => {
  return (
    <div
      className="ImageLogoOverlay-container"
      style={{
        backgroundImage: `url(${imageUrl})`,
      }}
    >
      <div
        className="ImageLogoOverlay-logo"
        style={{
          backgroundImage: `url(${logoUrl})`,
          width: logoWidth,
          height: logoHeight,
        }}
      />
    </div>
  );
};

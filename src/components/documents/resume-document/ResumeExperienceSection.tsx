import { FC, PropsWithChildren, ReactNode } from 'react';
import { Box, Stack, Typography, useTheme } from '@mui/material';
import { TransdimensionalRect, NoWrap } from '../..';
import { ResumeSection, ResumeSectionLabel } from '.';

const ResumeList: FC<PropsWithChildren<{}>> = ({ children }) => {
  const theme = useTheme();

  return (
    <Box
      component="ul"
      className="ResumeList-root"
      sx={{
        position: 'relative',
        paddingLeft: 2,
        margin: 0,
        // listStyle: 'none',
        '& .ResumeList-root': {
          '&::before': {
            content: "''",
            position: 'absolute',
            left: `calc(${theme.spacing(-2)} + 2px)`,
            top: -12,
            width: 2,
            height: 'calc(100% - 2px)',
            backgroundColor: theme.palette.text.secondary,
          },
          '& > .ResumeListItem-root': {
            '&::before': {
              content: "''",
              position: 'absolute',
              left: `calc(${theme.spacing(-4)} + 2px)`,
              top: 16,
              width: `calc(${theme.spacing(2)} - 2px)`,
              height: 2,
              backgroundColor: theme.palette.text.secondary,
            },
            '&::after': {
              content: "''",
              position: 'absolute',
              left: theme.spacing(-2),
              top: 14,
              width: 6,
              height: 6,
              backgroundColor: 'transparent',
              border: '2px solid',
              borderColor: theme.palette.text.secondary,
            },
          },
        },
        '& > .ResumeListItem-root': {
          listStyleType: 'square',
        },
      }}
    >
      {children}
    </Box>
  );
};

const ResumeListItem: FC<PropsWithChildren<{ label: ReactNode }>> = ({
  label,
  children,
}) => {
  return (
    <Box
      component="li"
      className="ResumeListItem-root"
      sx={{ position: 'relative' }}
    >
      <Typography component="span" variant="body1" lineHeight="inherit">
        {label}
      </Typography>
      {children}
    </Box>
  );
};

const ResumeExperienceItem: FC<{
  id: string;
  organization: ReactNode;
  qualifier?: ReactNode;
  location: ReactNode;
  time: ReactNode;
  role: ReactNode;
  description: ReactNode;
}> = (props) => {
  return (
    <TransdimensionalRect
      id={`resumeDocument_experienceItem_${props.id}`}
      display="block"
      sx={{ padding: 0 }}
    >
      <Typography
        component="span"
        variant="body1"
        fontWeight="bold"
        lineHeight="inherit"
      >
        {props.organization}
      </Typography>
      <Typography component="span" variant="body2" lineHeight="inherit">
        {' '}
        {props.qualifier ? '— ' + props.qualifier : ''} — {props.location} —{' '}
        {props.time}
      </Typography>
      <Typography
        variant="body1"
        fontWeight={600}
        fontStyle="italic"
        lineHeight="inherit"
      >
        {props.role}
      </Typography>
      {props.description}
    </TransdimensionalRect>
  );
};

export const ResumeExperienceSectionPage1: FC = () => {
  return (
    <ResumeSection
      sideContent={
        <ResumeSectionLabel>
          <NoWrap>Experience</NoWrap>
        </ResumeSectionLabel>
      }
    >
      <Stack spacing={1}>
        <ResumeExperienceItem
          id="geekPowered"
          organization="Geek Powered"
          // qualifier="Contract Employee"
          location="Madison, WI"
          time="November 2024 to Present"
          role="Director of Software Engineering & Digital Strategy"
          description={
            <ResumeList>
              <ResumeListItem label="Implementing digital transformations, responsive websites, and e-commerce storefronts with improved SEO for small business clients." />
            </ResumeList>
          }
        />
        <ResumeExperienceItem
          id="ryanBrothersAmbulance"
          organization="Ryan Brothers Ambulance"
          location="Madison, WI"
          time="February 2024 to October 2024"
          role="Paramedic"
          description={
            <ResumeList>
              <ResumeListItem label="Provided advanced life support to patients in pre-hospital and inter-facility settings." />
              <ResumeListItem label="Utilized state of the art equipment including Alaris Infusion pumps, Vapotherm, BiPAP, and blood products while delivering high quality patient care." />
            </ResumeList>
          }
        />
        <ResumeExperienceItem
          id="crossPlainsEms"
          organization="Cross Plains EMS"
          location="Cross Plains, WI"
          time="June 2023 to January 2024"
          role="Emergency Medical Technician"
          description={
            <ResumeList>
              <ResumeListItem label="Responded to 911 calls and provided emergency medical care as an ambulance crew member with an Advanced EMT level service." />
              {/* <ResumeListItem label="Assisted with department needs including monthly training and community outreach." /> */}
            </ResumeList>
          }
        />
      </Stack>
    </ResumeSection>
  );
};

export const ResumeExperienceSectionPage2: FC = () => {
  return (
    <ResumeSection sideContent={null}>
      <Stack spacing={2}>
        <ResumeExperienceItem
          id="dottid"
          organization="Dottid"
          location="Remote"
          time="June 2022 to March 2023"
          role="Senior Software Engineer"
          description={
            <ResumeList>
              <ResumeListItem label="Led frontend software development of Dottid Insights, achieving successful launch of a business intelligence tool developed for the commercial real estate industry." />
              <ResumeListItem label="Bridged the gap between engineering, product, and design teams by managing internal Storybook UI component library, significantly improving communication about UI development with non-engineers." />
              {/* <ResumeListItem label="Utilized modern web technologies to transform complex lease data into comprehensive property- and suite-level metrics, including occupancy, utilization, and churn analysis, improving decision-making of real estate brokerage clients." /> */}
              <ResumeListItem label="Enhanced decision-making of real estate brokerage clients by leveraging modern web technologies to transform complex lease data into comprehensive property- and suite-level metrics including occupancy, utilization, and churn rates." />
            </ResumeList>
          }
        />
        <ResumeExperienceItem
          id="undigital"
          organization="UnDigital"
          location="Remote"
          time="June 2021 to March 2022"
          role="Software Engineer"
          description={
            <ResumeList>
              <ResumeListItem label="Spearheaded development of an internal admin application used by customer support that enabled faster new client onboarding during a period of rapid growth." />
              {/* <ResumeListItem label="Worked with an early-revenue startup engineering team to develop the company's software controlled on-demand printed marketing solution." /> */}
              <ResumeListItem label="Implemented an internal solution to unify inventory management, print queues, and remote printer telemetry data, automating the replenishment of consumable ink and print media at customer sites." />
            </ResumeList>
          }
        />
        <ResumeExperienceItem
          id="gettyImages"
          organization="Getty Images"
          location="Madison, WI"
          time="June 2019 to March 2021"
          role="Software Engineer"
          description={
            <ResumeList>
              <ResumeListItem label="Significantly improved Electron-based application performance utilizing performance profiling, enabling field editors to edit and upload images faster and more reliably." />
              <ResumeListItem label="Reduced reliance on external video transcoding service by improving in-house functionality around uploading, storing, and processing videos in AWS, resulting in significant cost savings." />
              <ResumeListItem label="Implemented automated testing solutions to ensure everything that shipped was reliable and bug-free, including unit tests, browser-based testing with Cypress, and continuous integration in GitLab CI." />
            </ResumeList>
          }
        />
        <ResumeExperienceItem
          id="hardinDesignAndDevelopment"
          organization="Hardin Design and Development"
          location="Madison, WI"
          time="August 2018 to March 2019"
          role="Web Applications Developer"
          description={
            <ResumeList>
              <ResumeListItem label="Designed and maintained PHP-based web applications for major commercial real estate and insurance firms, delivering solutions tailored to client needs." />
              <ResumeListItem label="Developed interactive tools using React to visualize business intelligence data, empowering clients to make informed real estate purchasing decisions." />
              <ResumeListItem label="Consulted directly with clients to identify project goals and requirements, ensuring delivered applications aligned with business objectives." />
            </ResumeList>
          }
        />
      </Stack>
    </ResumeSection>
  );
};

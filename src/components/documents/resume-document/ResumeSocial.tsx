import { FC, ReactNode } from 'react';
import { Link } from 'react-router-dom';
import { useTheme } from '@mui/material';
import { ResumeSection } from '.';
import { Chip, ChipList, ChipThree, TransdimensionalRect } from '../..';
import LinkedInColorIcon from '../../../@mui/icons-material/LinkedInColor';
import GitLabColorIcon from '../../../@mui/icons-material/GitLabColor';
import GitHubColorIcon from '../../../@mui/icons-material/GitHubColor';
import SamuelGoodellColorIcon from '../../../@mui/icons-material/SamuelGoodellColor';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';

const RESUME_SOCIAL_ITEMS: Array<{
  id: string;
  label: string;
  url: string;
  icon?: ReactNode;
}> = [
  {
    id: 'linkedin',
    label: 'LinkedIn',
    url: 'https://www.linkedin.com/in/samuel-goodell-5aa8631aa/',
    icon: <LinkedInColorIcon />,
  },
  {
    id: 'gitlab',
    label: 'GitLab',
    url: 'https://gitlab.com/goodells',
    icon: <GitLabColorIcon />,
  },
  {
    id: 'github',
    label: 'GitHub',
    url: 'https://github.com/samuelgoodell',
    icon: <GitHubColorIcon />,
  },
  {
    id: 'portfolio',
    label: 'Portfolio',
    url: 'https://samuelgoodell.com',
    icon: <SamuelGoodellColorIcon />,
  },
  {
    id: 'email',
    label: 'sam@mangane.se',
    url: 'mailto:sam@mangane.se',
    icon: (
      <EmailOutlinedIcon
        sx={{
          position: 'relative',
          top: 0.5,
          transform: 'scale(1.1)',
        }}
      />
    ),
  },
];

const getSocialItemRectId = (socialItemId: string) =>
  `resumeDocument_socialItem_${socialItemId}`;

export const ResumeSocialThree: FC = () => {
  return RESUME_SOCIAL_ITEMS.map((itemProps) => {
    return (
      <ChipThree
        key={itemProps.id}
        id={getSocialItemRectId(itemProps.id)}
        depth={0.15}
      />
    );
  });
};

export const ResumeSocial: FC = () => {
  const theme = useTheme();

  return (
    <ResumeSection>
      <ChipList
        sx={{
          paddingBottom: 2,
        }}
      >
        {RESUME_SOCIAL_ITEMS.map((itemProps) => {
          return (
            <TransdimensionalRect
              key={itemProps.id}
              id={getSocialItemRectId(itemProps.id)}
              display="inline-block"
            >
              <Link to={itemProps.url} target="_blank">
                <Chip
                  key={itemProps.id}
                  background={theme.palette.text.primary}
                  startAdornment={itemProps.icon}
                >
                  {itemProps.label}
                </Chip>
              </Link>
            </TransdimensionalRect>
          );
        })}
      </ChipList>
    </ResumeSection>
  );
};

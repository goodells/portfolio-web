import { FC, useMemo } from 'react';
import { Color, MeshStandardMaterial } from 'three';
import { Box, Typography, useTheme } from '@mui/material';
import {
  ThreeText,
  ThreeTransdimensionalRect,
  TransdimensionalRect,
} from '../..';
import { ResumeContainer } from '.';
import { BoxCornerColors } from '../../../types';

const HERO_TEXT_DEPTH = 0.1;
const HERO_TEXT_DEPTH_RATIO = 0.4;
const HERO_TEXT_SCALE = 0.9;
const HERO_TEXT_OFFSET_Y = 0.0075;

export const ResumeHeroThree: FC = () => {
  const theme = useTheme();
  const heroTextBoxColors: BoxCornerColors = useMemo(() => {
    const brandRedColor = new Color(1, 0, 0);
    const brandOrangeColor = new Color(theme.palette.brandOrange.main);
    const brandYellowColor = new Color(1, 0.9, 0);

    return {
      frontBottomLeft: brandRedColor,
      frontBottomRight: brandYellowColor,
      frontTopLeft: brandRedColor.clone().lerp(brandOrangeColor, 0.3),
      frontTopRight: brandYellowColor.clone().lerp(brandOrangeColor, 0.1),
      backBottomLeft: brandRedColor.clone().multiplyScalar(0.75),
      backBottomRight: brandYellowColor
        .clone()
        .lerp(brandOrangeColor, 0.3)
        .multiplyScalar(0.65),
      backTopLeft: brandRedColor
        .clone()
        .lerp(brandOrangeColor, 0.1)
        .multiplyScalar(0.65),
      backTopRight: brandYellowColor.clone().lerp(brandOrangeColor, 0.3),
    };
  }, [theme]);
  const heroTextMaterial = useMemo(
    () =>
      new MeshStandardMaterial({
        vertexColors: true,
        color: 0xffffff,
      }),
    []
  );

  return (
    <ThreeTransdimensionalRect id="resumeDocument_heroText">
      <ThreeText
        fontId="OpenSans_ExtraBold"
        text="Sam"
        position={[
          -0.505,
          -0.192 + (0.365 * (1 - HERO_TEXT_SCALE)) / 2 + HERO_TEXT_OFFSET_Y,
          -1 * HERO_TEXT_DEPTH_RATIO * HERO_TEXT_DEPTH,
        ]}
        scale={[0.365, 0.365 * HERO_TEXT_SCALE, HERO_TEXT_DEPTH / 2]}
        vertexColors
        color={heroTextBoxColors}
        material={heroTextMaterial}
      />
    </ThreeTransdimensionalRect>
  );
};

export const ResumeHero: FC = () => {
  const theme = useTheme();

  return (
    <ResumeContainer>
      <Typography
        variant="h1"
        fontSize="4rem"
        lineHeight="1.5"
        sx={{ position: 'relative' }}
      >
        <TransdimensionalRect
          id="resumeDocument_heroText"
          sx={{
            position: 'absolute',
            left: 0,
            top: 'calc(100% * -1 / 6)',
            height: 'calc(100% * (4 / 3))',
            aspectRatio: '1 / 1',
            pointerEvents: 'none',
          }}
        />
        <Box
          component="span"
          sx={{
            color: 'transparent',
          }}
        >
          Sam
        </Box>
        uel Goodell
        <Box
          component="span"
          sx={{
            marginLeft: 2,
            fontSize: '1.5rem',
            color: theme.palette.text.disabled,
          }}
        >
          BSc, NREMT-P
        </Box>
      </Typography>
    </ResumeContainer>
  );
};

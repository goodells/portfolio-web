import { FC, Fragment, useState } from 'react';
import { Box, useTheme } from '@mui/material';
import { Vector2 } from 'three';
import { pointsToString } from '../../utilities';

import './ScrollDownIndicator.scss';
import { useScreenRangeEffect } from '../../hooks';

const SCROLL_DOWN_INDICATOR_COUNT = 3;
const SCROLL_DOWN_INDICATOR_STROKE_WIDTH = 6;
const SCROLL_DOWN_INDICATOR_SIZE = new Vector2(48, 24);
const SCROLL_DOWN_INDICATOR_BOTTOM_SPACING = 24;
const SCROLL_DOWN_INDICATOR_SPACING = 24;

export const ScrollDownIndicator: FC = () => {
  const theme = useTheme();
  const windowSize = new Vector2(window.innerWidth, window.innerHeight);
  const [ratio, setRatio] = useState<number | null>(1);

  useScreenRangeEffect({
    fromScreenY: 0,
    toScreenY: 100,
    onTriggerRatio: setRatio,
  });

  const isVisible = typeof ratio === 'number' && ratio < 0.1;
  const renderIndicator = (index: number) => {
    if (ratio === null) return null;

    const indicatorCenterbottom = new Vector2(
      windowSize.x / 2,
      windowSize.y -
        SCROLL_DOWN_INDICATOR_BOTTOM_SPACING -
        ratio * (125 + index * 10) -
        index * SCROLL_DOWN_INDICATOR_SPACING
    );
    const indicatorSize = SCROLL_DOWN_INDICATOR_SIZE.clone().multiply(
      new Vector2(
        1 + ratio * 2 * ((index + 1) / SCROLL_DOWN_INDICATOR_COUNT),
        1 - (ratio * ((index + 1) / SCROLL_DOWN_INDICATOR_COUNT)) / 1.5
      )
    );

    const indicatorPoints = pointsToString([
      indicatorCenterbottom
        .clone()
        .add(new Vector2(-indicatorSize.x / 2, -indicatorSize.y)),
      indicatorCenterbottom.clone().add(new Vector2(0, 0)),
      indicatorCenterbottom
        .clone()
        .add(new Vector2(indicatorSize.x / 2, -indicatorSize.y)),
    ]);

    return (
      <Fragment key={index}>
        <polyline
          points={indicatorPoints}
          fill="none"
          stroke={theme.palette.text.secondary}
          opacity={0.25}
        />
        <polyline
          points={indicatorPoints}
          fill="none"
          stroke={theme.palette.secondary.main}
          style={{
            opacity: 0,
            transition: 'opacity 0.4s',
            animation: isVisible
              ? `hideshow 10s ${
                  10000 +
                  ((SCROLL_DOWN_INDICATOR_COUNT - 1 - index) * 500) /
                    SCROLL_DOWN_INDICATOR_COUNT
                }ms infinite`
              : '',
          }}
        />
      </Fragment>
    );
  };

  return (
    <Box
      component="div"
      sx={{
        position: 'fixed',
        left: 0,
        top: 0,
        width: '100vw',
        height: '100vh',
        pointerEvents: 'none',
        zIndex: 50,
        transition: 'opacity 0.4s',
        ...(typeof ratio === 'number' && {
          opacity: isVisible ? 1 : 0,
        }),
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox={`0 0 ${windowSize.x} ${windowSize.y}`}
      >
        <g
          stroke="white"
          strokeLinecap="butt"
          strokeWidth={SCROLL_DOWN_INDICATOR_STROKE_WIDTH}
        >
          {[...Array(SCROLL_DOWN_INDICATOR_COUNT).keys()].map((_, index) =>
            renderIndicator(index)
          )}
        </g>
      </svg>
    </Box>
  );
};

import { FC, PropsWithChildren } from 'react';

export const StaticEffectMask: FC<PropsWithChildren<{}>> = ({ children }) => {
  return (
    <div className="StaticEffectMask">
      <div className="StaticEffectMask-content">{children}</div>
    </div>
  );
};

import { FC, PropsWithChildren } from 'react';
import { MDXProvider } from '@mdx-js/react';
import { MDXComponents } from 'mdx/types';
import { Typography, Divider } from '@mui/material';
import { InlineMath, BlockMath } from 'react-katex';

import { Breadcrumbs, InlineLink } from '..';

export const ApplicationMDXProvider: FC<PropsWithChildren<{}>> = ({
  children,
}) => {
  const mdxComponents: MDXComponents = {
    ...(['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] as const).reduce(
      (components, variant) => ({
        ...components,
        [variant]: (props: any) => {
          return <Typography variant={variant} {...props} />;
        },
      }),
      {}
    ),
    p: (props: any) => {
      return <Typography variant="body1" {...props} />;
    },
    em: (props: any) => {
      return <Typography display="inline" fontStyle="italic" {...props} />;
    },
    strong: (props: any) => {
      return <Typography display="inline" fontWeight="bold" {...props} />;
    },
    del: (props: any) => {
      return (
        <Typography
          display="inline"
          textDecoration="strikethrough"
          {...props}
        />
      );
    },
    a: (props: any) => {
      return <InlineLink to={props.href ?? ''} {...props} />;
    },
    hr: (props: any) => {
      return <Divider sx={{ marginTop: 2, marginBottom: 2 }} {...props} />;
    },
    img: (props: any) => {
      return <img {...props} style={{ maxWidth: '100%' }} />;
    },
    Breadcrumbs,
    Math: (props: { display?: boolean; children: string }) => {
      if (props.display) {
        return <BlockMath>{props.children}</BlockMath>;
      } else {
        return <InlineMath math={props.children} />;
      }
    },
  };

  return <MDXProvider components={mdxComponents}>{children}</MDXProvider>;
};

import { ReactNode, FC, useEffect, useState, useContext } from 'react';
import * as runtime from 'react/jsx-runtime';
import { evaluate } from '@mdx-js/mdx';
import { useMDXComponents } from '@mdx-js/react';
import type { MDXProps } from 'mdx/types';
import remarkMath from 'remark-math';
import remarkMdxEnhanced from 'remark-mdx-math-enhanced';
import 'katex/dist/katex.min.css';

import { Box } from '@mui/material';
import { ProgressContext } from '../../contexts';

type ReactMDXContent = (props: MDXProps) => ReactNode;

export const RemoteMDX: FC<{
  sourceUrl: string;
}> = ({ sourceUrl }) => {
  const progressContext = useContext(ProgressContext);

  const [evaluated, setEvaluated] = useState(false);
  const [mdxSource, setMdxSource] = useState<string | null>(null);
  const [MdxContent, setMdxContent] = useState<ReactMDXContent>(
    () => () => null
  );

  useEffect(() => {
    if (!mdxSource) return;

    progressContext.setActive(true);

    evaluate(mdxSource, {
      ...runtime,
      remarkPlugins: [remarkMath, [remarkMdxEnhanced, { component: 'Math' }]],
    })
      .then((result) => {
        setMdxContent(() => result.default);
        setEvaluated(true);
      })
      .finally(() => {
        progressContext.setActive(false);
      });
  }, [mdxSource]);

  useEffect(() => {
    fetch(sourceUrl)
      .then((response) => {
        return response.text();
      })
      .then((responseBody) => {
        setMdxSource(responseBody);
      });
  }, [sourceUrl]);

  const mdxComponents = useMDXComponents();

  return (
    <Box
      component="div"
      sx={{
        position: 'relative',
        opacity: evaluated ? 1 : 0,
        transition: 'opacity 0.4s',
      }}
    >
      <MdxContent components={mdxComponents} />
    </Box>
  );
};

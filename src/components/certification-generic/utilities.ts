export const getCertificationGenericHeaderRectId = (id: string) =>
  `${id}-header`;
export const getCertificationGenericBodyRectId = (id: string) => `${id}-body`;

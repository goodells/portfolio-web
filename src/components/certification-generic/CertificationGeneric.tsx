import { FC, PropsWithChildren, ReactNode } from 'react';
import classNames from 'classnames';
import { Stack, Typography } from '@mui/material';
import { TransdimensionalRect } from '..';
import {
  getCertificationGenericHeaderRectId,
  getCertificationGenericBodyRectId,
} from './utilities';

export const CertificationGeneric: FC<
  PropsWithChildren<{
    id: string;
    icon?: ReactNode;
    label: ReactNode;
    level: ReactNode;
  }>
> = ({ id, icon, label, level, children }) => {
  const headerRectId = getCertificationGenericHeaderRectId(id);
  const bodyRectId = getCertificationGenericBodyRectId(id);

  return (
    <TransdimensionalRect
      id={id}
      className={classNames('Certification', 'CertificationGeneric')}
    >
      <TransdimensionalRect
        id={headerRectId}
        className="CertificaitonGeneric-header"
        display="block"
        sx={{
          position: 'relative',
          paddingTop: '50%',
          textAlign: 'center',
        }}
      >
        <Stack
          direction="row"
          spacing={1}
          justifyContent="center"
          alignItems="center"
          sx={{
            position: 'absolute',
            left: '10%',
            top: '20%',
            width: '80%',
          }}
        >
          {icon}
          <Typography variant="body1" fontWeight="bold" lineHeight="1.8rem">
            {label}
          </Typography>
        </Stack>
        <Typography
          variant="body2"
          sx={{
            position: 'absolute',
            left: '10%',
            top: '75%',
            width: '80%',
          }}
        >
          {level}
        </Typography>
      </TransdimensionalRect>
      <TransdimensionalRect
        id={bodyRectId}
        className="Certification-body"
        display="block"
        sx={{
          padding: 1.5,
          paddingTop: 2,
        }}
      >
        {children}
      </TransdimensionalRect>
    </TransdimensionalRect>
  );
};

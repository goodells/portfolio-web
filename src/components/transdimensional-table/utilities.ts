export const getTransdimensionalTableHeadRectId = (tableId: string) =>
  `${tableId}-head`;

export const getTransdimensionalTableRowRectId = (
  tableId: string,
  rowId: string
) => `${tableId}-row[${rowId}]`;

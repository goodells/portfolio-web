import { FC } from 'react';
import ReactMarkdown from 'react-markdown';
import remarkMath from 'remark-math';
import { Typography } from '@mui/material';
import { InlineLink } from '../inline-link';

export const PresentationMarkdown: FC<{
  markdown: string;
}> = ({ markdown }) => {
  return (
    <ReactMarkdown
      remarkPlugins={[remarkMath]}
      components={{
        ...(['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] as const).reduce(
          (components, variant) => ({
            ...components,
            [variant]: ({ node, ...props }: any) => {
              return <Typography variant={variant} {...props} />;
            },
          }),
          {}
        ),
        p: ({ node, ...props }) => {
          return <Typography variant="body1" {...props} />;
        },
        a: ({ node, ...props }) => {
          return <InlineLink to={props.href ?? ''} {...props} />;
        },
        img: ({ node, ...props }) => {
          return <img {...props} style={{ maxWidth: '100%' }} />;
        },
      }}
    >
      {markdown}
    </ReactMarkdown>
  );
};

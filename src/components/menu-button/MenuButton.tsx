import { FC, useEffect, useState } from 'react';
import classNames from 'classnames';
import { useTheme } from '@mui/material';

import './MenuButton.scss';

export const MenuButton: FC = () => {
  const theme = useTheme();
  const [scrollY, setScrollY] = useState<number>(window.scrollY);

  useEffect(() => {
    const handleScroll = () => {
      setScrollY(window.scrollY);
    };

    window.addEventListener('scroll', handleScroll, { passive: true });

    return () => window.removeEventListener('scroll', handleScroll);
  }, [setScrollY]);

  const enabled = scrollY > 200;
  const enabling = scrollY > 150;
  const enablingRatio = (scrollY - 150) / (200 - 150);

  return (
    <div
      className="MenuButton"
      style={{
        ...(() => {
          switch (true) {
            case enabled:
              return {
                opacity: 1,
                top: 25,
              };
            case enabling:
              return {
                top: 25,
                opacity: enablingRatio,
              };
            default:
              return {
                opacity: 0,
              };
          }
        })(),
      }}
    >
      <div
        className="MenuButton-background"
        style={{
          background: `linear-gradient(135deg, ${theme.palette.brandRed.main} 0%, ${theme.palette.brandOrange.main} 50%, ${theme.palette.brandYellow.main} 100%)`,
        }}
      />
      <div className={classNames('MenuButton-line', 'MenuButton-line_top')} />
      <div
        className={classNames('MenuButton-line', 'MenuButton-line_middle')}
      />
      <div
        className={classNames('MenuButton-line', 'MenuButton-line_bottom')}
      />
    </div>
  );
};

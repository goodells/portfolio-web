export const getCertificationAwsHeaderRectId = (id: string) => `${id}-header`;
export const getCertificationAwsBodyRectId = (id: string) => `${id}-body`;

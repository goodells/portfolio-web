import { FC } from 'react';
import { TransdimensionalUnitConversionServiceContext } from '../../../contexts';
import {
  MeetingCreationThree,
  PageHeaderThree,
  Three,
  ThreeLighting,
  ThreeScrollOffsetContainer,
} from '../..';

export const MeetPageThree: FC = () => {
  return (
    <Three
      id="reference_page_three"
      inset
      sx={{
        position: 'absolute',
        width: '100%',
        height: '100%',
      }}
    >
      <ThreeScrollOffsetContainer>
        <ThreeLighting />
        <TransdimensionalUnitConversionServiceContext.Consumer>
          {(transdimensionalUnitConversionService) => {
            if (!transdimensionalUnitConversionService) return null;

            return (
              <>
                <PageHeaderThree />
                <MeetingCreationThree id="" />
              </>
            );
          }}
        </TransdimensionalUnitConversionServiceContext.Consumer>
      </ThreeScrollOffsetContainer>
    </Three>
  );
};

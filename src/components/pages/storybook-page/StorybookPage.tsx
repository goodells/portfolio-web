import { FC } from 'react';
import { Box, Typography } from '@mui/material';
import { Section, PageHeader, Container, Breadcrumbs } from '../..';
import { StorybookPageThree } from '.';

export const StorybookPage: FC = () => {
  return (
    <>
      <StorybookPageThree />
      <Section id="storybookPage_headerSection">
        <Container maxWidth="lg">
          <PageHeader variant="search" />
        </Container>
      </Section>
      <Section id="storybookPage_mainSection">
        <Container maxWidth="lg">
          <Breadcrumbs
            variant="pageHeader"
            prefixBrandCircle
            maxItems={3}
            items={[
              {
                to: '/storybook',
                text: 'Storybook',
              },
            ]}
          />
          <Typography variant="h2">Typography</Typography>
        </Container>
      </Section>
      <Box component="div" sx={{ height: '200vh' }} />
    </>
  );
};

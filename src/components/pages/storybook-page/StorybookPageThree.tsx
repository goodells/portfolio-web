import { FC } from 'react';
import {
  PageHeaderThree,
  Three,
  ThreeLighting,
  ThreeScrollOffsetContainer,
} from '../..';

export const StorybookPageThree: FC = () => {
  return (
    <Three
      id="storybookPage_headerSection-three"
      interactive={false}
      sx={{
        position: 'absolute',
        width: '100%',
        height: '100%',
      }}
    >
      <ThreeScrollOffsetContainer>
        <ThreeLighting />
        <PageHeaderThree variant="search" />
      </ThreeScrollOffsetContainer>
    </Three>
  );
};

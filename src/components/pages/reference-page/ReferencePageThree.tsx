import { FC } from 'react';
import { TransdimensionalUnitConversionServiceContext } from '../../../contexts';
import { Three, ThreeLighting, ThreeScrollOffsetContainer } from '../..';

export const ReferencePageThree: FC = () => {
  return (
    <Three
      id="reference_page_three"
      interactive={false}
      fogFar={50}
      sx={{
        position: 'absolute',
        width: '100%',
        height: '100%',
        left: 0,
        top: 0,
      }}
    >
      <ThreeLighting />
      <ThreeScrollOffsetContainer>
        <TransdimensionalUnitConversionServiceContext.Consumer>
          {(transdimensionalUnitConversionService) => {
            if (!transdimensionalUnitConversionService) return null;

            return <></>;
          }}
        </TransdimensionalUnitConversionServiceContext.Consumer>
      </ThreeScrollOffsetContainer>
    </Three>
  );
};

export default ReferencePageThree;

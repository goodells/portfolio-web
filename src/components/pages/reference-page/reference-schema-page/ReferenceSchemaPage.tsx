import { FC, useEffect, useRef } from 'react';
import { useParams } from 'react-router-dom';
import { Box, Container, Typography } from '@mui/material';
import { Breadcrumbs, Section, TransdimensionalRect } from '../../..';
import {
  useTransdimensionalOffsetLink,
  useTransdimensionalRectLink,
} from '../../../../hooks';
import { ReferenceSchemaPageThree } from '.';

import AceEditor from 'react-ace';

import 'ace-builds/src-noconflict/mode-json';
import 'ace-builds/src-noconflict/theme-monokai';
import 'ace-builds/src-noconflict/ext-language_tools';

export const ReferenceSchemaPage: FC = () => {
  const { schemaId } = useParams();
  const contentElementRef = useRef<HTMLElement | null>(null);
  const gutterTransdimensionalRectLink = useTransdimensionalRectLink({
    id: 'aceEditorTest-gutter',
  });
  const contentTransdimensionalOffsetLink = useTransdimensionalOffsetLink({
    z: -0.1,
    handleTransform: (transform) => {
      const contentElement = contentElementRef.current;

      if (!contentElement) return;

      contentElement.style.transform = transform;
    },
  });

  useEffect(() => {
    return () => {
      gutterTransdimensionalRectLink.unlink();
      contentTransdimensionalOffsetLink.unlink();
    };
  }, [
    gutterTransdimensionalRectLink.unlink,
    contentTransdimensionalOffsetLink.unlink,
  ]);

  return (
    <Section
      id="referenceSchemaPage_indexSection"
      three={<ReferenceSchemaPageThree />}
    >
      <Container maxWidth="lg">
        <Breadcrumbs
          variant="pageHeader"
          prefixBrandCircle
          maxItems={3}
          items={[
            {
              to: '/reference',
              text: 'Reference',
            },
            {
              to: '/reference/schemas',
              text: 'Schemas',
            },
            {
              to: `/reference/schemas/${schemaId}`,
              text: 'OwnTracks Log Message',
            },
          ]}
        />
        <Typography variant="body1">
          A geographical coordinate on a planet (most commonly Earth).
        </Typography>
        <br />
        <TransdimensionalRect id="aceEditorTest-container">
          <AceEditor
            mode="json"
            theme="monokai"
            onChange={(_newValue) => {}}
            onLoad={(editor) => {
              const gutterElements =
                editor.renderer.container.getElementsByClassName('ace_gutter');

              if (gutterElements.length !== 1) {
                console.warn('Ace gutter was not found.');

                return;
              }

              const gutterElement = gutterElements[0] as HTMLDivElement;

              gutterTransdimensionalRectLink.link(gutterElement);

              const contentElements =
                editor.renderer.container.getElementsByClassName('ace_content');

              if (contentElements.length !== 1) {
                console.warn('Ace content was not found.');

                return;
              }

              const contentElement = contentElements[0]
                .parentElement as HTMLDivElement;

              contentElementRef.current = contentElement;

              contentTransdimensionalOffsetLink.link(contentElement);

              // const gutterLayerElements =
              //   gutterElement.getElementsByClassName('ace_gutter-layer');

              // if (gutterLayerElements.length !== 1) {
              //   console.warn('Ace gutter layer was not found.');

              //   return;
              // }

              // const gutterLayerElement = gutterLayerElements[0] as HTMLDivElement;

              // transdimensionalOffsetLink.link(gutterLayerElement);
            }}
            onCursorChange={(selection, _event) => {
              console.log(selection);
            }}
            name="UNIQUE_ID_OF_DIV"
            editorProps={{ $blockScrolling: true }}
            setOptions={{ useWorker: false }}
            width="100%"
            style={
              {
                // opacity: 0.5,
              }
            }
            minLines={-1}
            maxLines={Infinity}
            showPrintMargin={false}
            tabSize={2}
            value={JSON.stringify(
              {
                $id: 'https://example.com/geographical-location.schema.json',
                $schema: 'https://json-schema.org/draft/2020-12/schema',
                title: 'Longitude and Latitude',
                description:
                  'A geographical coordinate on a planet (most commonly Earth).',
                required: ['latitude', 'longitude'],
                type: 'object',
                properties: {
                  latitude: {
                    type: 'number',
                    minimum: -90,
                    maximum: 90,
                  },
                  longitude: {
                    type: 'number',
                    minimum: -180,
                    maximum: 180,
                  },
                },
              },
              null,
              2
            )}
          />
        </TransdimensionalRect>
      </Container>
      <Box component="div" sx={{ height: 2000 }} />
    </Section>
  );
};

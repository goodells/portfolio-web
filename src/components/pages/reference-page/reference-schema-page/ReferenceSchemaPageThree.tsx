import { FC } from 'react';
import {
  Three,
  ThreeBox,
  ThreeLighting,
  ThreeScrollOffsetContainer,
  ThreeTransdimensionalRect,
} from '../../..';
import { useMaterial } from '../../../../hooks';

export const ReferenceSchemaPageThree: FC = () => {
  const gutterMaterial = useMaterial('rgb(67, 69, 58)');
  const gutterActiveLineMaterial = useMaterial('rgb(51, 51, 51)');
  const contentMaterial = useMaterial('rgb(53, 55, 47)');

  const contentDepth = 0.1;
  const containerDepth = 0.2;
  const gutterOverlap = 0.00001;
  const gutterActiveLineOverlap = 0.00002;

  const editorLinesCount = 23;
  const editorActiveLineIndex = 9;

  return (
    <Three
      id="referenceSchemaPage-three"
      sx={{
        position: 'absolute',
        width: '100%',
        height: '100%',
      }}
    >
      <ThreeLighting />
      <ThreeScrollOffsetContainer>
        <ThreeTransdimensionalRect id="aceEditorTest-gutter">
          <ThreeBox
            position={[0, 0, -containerDepth / 2]}
            scale={[1 + gutterOverlap, 1 + gutterOverlap, containerDepth]}
            material={gutterMaterial}
          />
          <ThreeBox
            position={[
              0,
              -0.5 + (1 - (editorActiveLineIndex + 0.5) / editorLinesCount),
              -containerDepth / 2,
            ]}
            scale={[
              1 + gutterActiveLineOverlap,
              1 / editorLinesCount,
              containerDepth,
            ]}
            material={gutterActiveLineMaterial}
          />
        </ThreeTransdimensionalRect>
        <ThreeTransdimensionalRect id="aceEditorTest-container">
          <ThreeBox
            position={[0, 0, -(containerDepth + contentDepth) / 2]}
            scale={[1, 1, containerDepth - contentDepth]}
            material={contentMaterial}
          />
        </ThreeTransdimensionalRect>
      </ThreeScrollOffsetContainer>
    </Three>
  );
};

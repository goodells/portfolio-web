import { FC } from 'react';
import { Container } from '@mui/material';
import { Breadcrumbs, Section } from '../../..';
import { ReferenceTermsPageThree } from '.';

export const ReferenceTermsPage: FC = () => {
  return (
    <Section
      id="referenceTermsPage_indexSection"
      three={<ReferenceTermsPageThree />}
    >
      <Container maxWidth="lg">
        <Breadcrumbs
          variant="pageHeader"
          prefixBrandCircle
          items={[
            {
              to: '/reference',
              text: 'Reference',
            },
            {
              to: '/reference/terms',
              text: 'Terms',
            },
          ]}
        />
      </Container>
    </Section>
  );
};

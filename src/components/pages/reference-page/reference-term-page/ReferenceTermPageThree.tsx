import { FC } from 'react';
import { Three, ThreeLighting, ThreeScrollOffsetContainer } from '../../..';

export const ReferenceTermPageThree: FC = () => {
  return (
    <Three
      id="referenceTermPage-three"
      sx={{
        position: 'absolute',
        width: '100%',
        height: '100%',
      }}
    >
      <ThreeLighting />
      <ThreeScrollOffsetContainer>{/* todo */}</ThreeScrollOffsetContainer>
    </Three>
  );
};

import { FC } from 'react';
import { alpha, useTheme } from '@mui/material';
import { Typealong } from '../../..';

// Components constants
const RECENT_VALUES_COUNT = 10;
const VALUES = [
  'web apps with React',
  'progressive web apps',
  'games with Unity',
  'responsive websites',
  'useful web apps',
  'open source libraries',
  'smart home integrations',
  'plugins for Homebridge',
  '3D scenes with THREE.js',
  '3D printed parts',
  'small business websites',
  'geospatial applications',
  'WebGL applications',
  'asset pipelines',
  'RESTful web APIs',
  'powerful APIs',
  'scalable web services',
  'cloud infrastructure',
  'Terraform definitions',
  'AMIs with Packer',
  'secrets safe with Vault',
  'systemd units',
  'SQS queues',
  'lambda functions',
  'full stack solutions',
  'apps with Electron',
  'mobile apps with Ionic',
  'enterprise tooling',
  'internal business tools',
  'fun user experiences',
  'broken stuff work',
  'requirements documents',
  'others more productive',
  'friends on other teams',
  'auto-generated docs',
  'GLSL fragment shaders',
].map((value) => value + '.');

export const MainPageIntroductionTypealong: FC = () => {
  const theme = useTheme();

  return (
    <h2
      className="IntroductionTypealong"
      style={{
        color: alpha(theme.palette.text.secondary, 0.75),
      }}
    >
      Hi, my name is{' '}
      <span
        className="Typealong-name"
        style={{
          color: theme.palette.text.primary,
        }}
      >
        Sam
      </span>
      .
      <br />I make{' '}
      <Typealong
        values={VALUES}
        recentValuesCount={RECENT_VALUES_COUNT}
        classes={{
          valueContainer: {
            default: 'IntroductionTypealong-valueContainer',
          },
          value: {
            default: 'IntroductionTypealong-value',
          },
        }}
        styles={{
          value: {
            default: {
              color: theme.palette.text.primary,
            },
            selecting: {
              color: 'white',
            },
          },
          selectionBackground: {
            selecting: {
              background: `linear-gradient(135deg, ${theme.palette.brandRed.main} 0%, ${theme.palette.brandOrange.main} 50%, ${theme.palette.brandYellow.main} 100%)`,
            },
          },
        }}
      />
    </h2>
  );
};

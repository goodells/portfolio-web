import { FC, PropsWithChildren, ReactNode, useRef, useState } from 'react';
import Marquee from 'react-fast-marquee';
import { Box, Container, Typography, useTheme } from '@mui/material';
import {
  Breadcrumbs,
  Chip,
  Section,
  ThemeInverter,
  TransdimensionalRect,
  TransdimensionalOffset,
  Heading,
  ChipInlineWrapper,
  Three,
  ThreeBox,
  ThreeScrollOffsetContainer,
  ThreeLighting,
  InlineLink,
} from '../../..';

import { MainPageProjectsSectionThree } from '.';
import { TransdimensionalUnitConversionServiceContext } from '../../../../contexts';
import { Vector2 } from 'three';
import { useMaterial, useSpacing } from '../../../../hooks';

const MARQUEE_SPEED = 25;
const MARQUEE_HEIGHT = 224;

const ProjectsMarquee: FC<
  PropsWithChildren<{
    id: string;
    width: number;
    height: number;
    direction: 'left' | 'right';
    threeChildren: ReactNode;
  }>
> = ({ id, width, height, direction, threeChildren, children }) => {
  const marqueeRef = useRef<HTMLDivElement | null>(null);
  const [marqueePlaying, setMarqueePlaying] = useState(true);
  const marqueeProps = {
    play: marqueePlaying,
    direction,
    speed: MARQUEE_SPEED,
    gradient: false,
  };

  return (
    <Box
      component="div"
      sx={{ position: 'relative', height }}
      onMouseEnter={() => {
        setMarqueePlaying(false);
      }}
      onMouseLeave={() => {
        setMarqueePlaying(true);
      }}
    >
      <Box
        component="div"
        sx={{
          position: 'absolute',
          left: 0,
          top: 0,
          width: '100%',
          height: '100%',
          pointerEvents: 'none',
          '& > .rfm-marquee-container > .rfm-marquee:nth-child(1)': {
            '& .MarqueeThree_-1': {
              display: 'none',
            },
            '& .MarqueeThree_0': {
              // display: 'none',
            },
            '& .MarqueeThree_1': {
              // display: 'none',
            },
          },
          '& > .rfm-marquee-container > .rfm-marquee:nth-child(2)': {
            '& .MarqueeThree_-1': {
              display: 'none',
            },
            '& .MarqueeThree_0': {
              display: 'none',
            },
            '& .MarqueeThree_1': {
              display: 'none',
            },
          },
        }}
      >
        <Marquee {...marqueeProps} style={{ overflow: 'visible' }}>
          <Box
            component="div"
            sx={{
              position: 'relative',
              width,
              minWidth: '100vw',
              height: MARQUEE_HEIGHT,
            }}
          >
            {[0, 1].map((marqueeIndex) => (
              <Three
                key={marqueeIndex}
                id={`${id}_three_${marqueeIndex}`}
                className={`MarqueeThree_${marqueeIndex}`}
                interactive={false}
                sx={{
                  position: 'absolute',
                  transform: 'translateX(-25%)',
                  left: `calc(${marqueeIndex} * 100% + 0%)`,
                  top: 'calc((100% - 100vh) / 2)',
                  width: '150%',
                  height: '100vh',
                }}
              >
                <ThreeLighting />
                <ThreeScrollOffsetContainer
                  cameraFractionalOffset={new Vector2(marqueeIndex, 0)}
                  sceneFractionalOffset={new Vector2(marqueeIndex - 0.25, 0)}
                  frameloop
                >
                  <TransdimensionalUnitConversionServiceContext.Consumer>
                    {(transdimensionalUnitConversionService) => {
                      if (!transdimensionalUnitConversionService) return null;

                      const sceneSize =
                        transdimensionalUnitConversionService.getVisibleSizeAtDepth(
                          0
                        );

                      return (
                        <group position={[0, -sceneSize.y / 2, 0]}>
                          {threeChildren}
                        </group>
                      );
                    }}
                  </TransdimensionalUnitConversionServiceContext.Consumer>
                </ThreeScrollOffsetContainer>
              </Three>
            ))}
          </Box>
        </Marquee>
      </Box>
      <Marquee ref={marqueeRef} {...marqueeProps}>
        {children}
      </Marquee>
    </Box>
  );
};

export const MainPageProjectsSection: FC = () => {
  const theme = useTheme();
  const headingSx = {
    position: 'relative',
    zIndex: 1,
  };

  const cardSpacing = useSpacing(2);
  const landscapeVideoCardWidth = MARQUEE_HEIGHT * (1280 / 720);
  const largeCardWidth = landscapeVideoCardWidth;
  // const mediumCardWidth = MARQUEE_HEIGHT;

  const marqueeWidth =
    largeCardWidth + cardSpacing + largeCardWidth + cardSpacing;

  const card1Material = useMaterial('rgb(128, 128, 128)');
  const card2Material = useMaterial('rgb(32, 64, 84)');

  return (
    <Section
      id="section-projects"
      three={<MainPageProjectsSectionThree />}
      sx={{ overflow: 'hidden' }}
    >
      <Container maxWidth="md">
        <Heading variant="h2" id="my-projects" sx={headingSx}>
          My Projects
        </Heading>
        <Typography variant="h4" sx={{ ...headingSx, marginBottom: 2 }}>
          Home Automation
        </Typography>
      </Container>
      <ProjectsMarquee
        id="mainPage_projectsSection_homeAutomationMarquee"
        width={marqueeWidth}
        height={MARQUEE_HEIGHT}
        direction="left"
        threeChildren={
          <TransdimensionalUnitConversionServiceContext.Consumer>
            {(transdimensionalUnitConversionService) => {
              if (!transdimensionalUnitConversionService) return null;

              const cardSpacingThree =
                transdimensionalUnitConversionService.clientToThreeX(
                  cardSpacing
                );
              const cardHeightThree =
                transdimensionalUnitConversionService.clientToThreeY(
                  MARQUEE_HEIGHT
                );
              const card1WidthThree =
                transdimensionalUnitConversionService.clientToThreeX(
                  largeCardWidth
                );
              const card2WidthThree =
                transdimensionalUnitConversionService.clientToThreeX(
                  largeCardWidth
                );

              return (
                <>
                  <ThreeBox
                    position={[card1WidthThree / 2, 0, -theme.depth.card / 2]}
                    scale={[card1WidthThree, cardHeightThree, theme.depth.card]}
                    material={card1Material}
                  />
                  <ThreeBox
                    position={[
                      card1WidthThree + cardSpacingThree + card2WidthThree / 2,
                      0,
                      -theme.depth.card / 2,
                    ]}
                    scale={[card2WidthThree, cardHeightThree, theme.depth.card]}
                    material={card2Material}
                  />
                </>
              );
            }}
          </TransdimensionalUnitConversionServiceContext.Consumer>
        }
      >
        <Box
          component="div"
          sx={{
            position: 'relative',
            marginRight: 2,
            background: 'rgb(128, 128, 128)',
            height: MARQUEE_HEIGHT,
            width: largeCardWidth,
            overflow: 'hidden',
          }}
        >
          <Box
            component="div"
            sx={{
              position: 'absolute',
              right: 16,
              top: 16,
              width: '2rem',
              height: '2rem',
              backgroundImage: 'url(/images/homebridge-outline-white.svg)',
              backgroundPosition: 'center center',
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'contain',
            }}
          />
          <Box
            component="div"
            sx={{
              position: 'absolute',
              bottom: 0,
              padding: theme.spacing(0, 2),
              // background: 'linear-gradient(to top, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0) 100%)',
              height: '4.5em',
              width: '100%',
            }}
          >
            <Typography
              variant="body1"
              fontWeight={500}
              sx={{
                textShadow: '0px 0px 8px rgba(0, 0, 0, 0.5)',
              }}
            >
              <Typography display="inline" fontWeight="bold">
                Homebridge LIFX Multi
              </Typography>{' '}
              enables dynamic multi-color animated light effects with{' '}
              <InlineLink to="https://www.lifx.com">LIFX</InlineLink> lights.
            </Typography>
          </Box>
        </Box>
        <Box
          component="div"
          sx={{
            marginRight: cardSpacing + 'px',
            backgroundColor: 'rgb(32, 64, 84)',
            width: largeCardWidth,
            height: MARQUEE_HEIGHT,
          }}
        >
          <Typography variant="body1">
            <b>Foreverdaunt:8Bit</b> is a retro-themed online multiplayer
            sandbox game.
          </Typography>
        </Box>
      </ProjectsMarquee>
      {/* <br />
      <Container maxWidth="md">
        <Typography variant="h4" sx={{ ...headingSx, marginBottom: 2 }}>
          Parametric Modeling
        </Typography>
      </Container>
      <ProjectsMarquee
        id="mainPage_projectsSection_parametricModelingMarquee"
        width={
          MARQUEE_HEIGHT * (4 / 2) +
          cardSpacing +
          MARQUEE_HEIGHT * (2 / 2) +
          cardSpacing
        }
        height={MARQUEE_HEIGHT}
        direction="right"
        threeChildren={
          <TransdimensionalUnitConversionServiceContext.Consumer>
            {(transdimensionalUnitConversionService) => {
              if (!transdimensionalUnitConversionService) return null;

              const cardSpacingThree =
                transdimensionalUnitConversionService.clientToThreeX(
                  cardSpacing
                );
              const cardHeightThree =
                transdimensionalUnitConversionService.clientToThreeY(
                  MARQUEE_HEIGHT
                );
              const gridfinityGridUnitThree = cardHeightThree / 2;
              const gridfinityGridScaleThree = cardHeightThree / 42;
              const gridfinityModelScaleThree = 2 * gridfinityGridScaleThree;

              return (
                <group position={[0, 0, 0]}>
                  <ThreeModel
                    id="gridfinity_gillette_mach3_box"
                    position={[(4 * gridfinityGridUnitThree) / 2, 0, 0]}
                    rotation={[0, 0, Math.PI / 2]}
                    scale={[
                      gridfinityModelScaleThree,
                      gridfinityModelScaleThree,
                      gridfinityModelScaleThree,
                    ]}
                  />
                  <ThreeModel
                    id="gridfinity_airpods_pro_2_box"
                    position={[
                      4 * gridfinityGridUnitThree +
                        cardSpacingThree +
                        (2 * gridfinityGridUnitThree) / 2,
                      0,
                      0,
                    ]}
                    rotation={[0, 0, 0]}
                    scale={[
                      gridfinityModelScaleThree,
                      gridfinityModelScaleThree,
                      gridfinityModelScaleThree,
                    ]}
                  />
                </group>
              );
            }}
          </TransdimensionalUnitConversionServiceContext.Consumer>
        }
      >
        {[
          {
            id: 'gridfinity_gillette_mach3_box',
            // threeChildren:
            children: (
              <>
                <Box
                  component="div"
                  sx={{
                    position: 'absolute',
                    right: 24,
                    top: 16,
                    width: '2rem',
                    height: '2rem',
                    backgroundImage: 'url(/images/gridfinity-logo-white.svg)',
                    backgroundPosition: 'center center',
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'contain',
                  }}
                />
                <Typography
                  variant="body1"
                  fontWeight="bold"
                  sx={{
                    margin: theme.spacing(1, 2),
                    position: 'absolute',
                    bottom: 0,
                    textShadow: '0px 0px 12px rgba(0, 0, 0, 0.5)',
                  }}
                >
                  <InlineLink to="#">Gillette Mach3 Box</InlineLink>
                </Typography>
              </>
            ),
            gridSize: new Vector2(4, 2),
          },
          {
            id: 'gridfinity_airpods_pro_2_box',
            // threeChildren:
            children: (
              <>
                <Box
                  component="div"
                  sx={{
                    position: 'absolute',
                    right: 24,
                    top: 16,
                    width: '2rem',
                    height: '2rem',
                    backgroundImage: 'url(/images/gridfinity-logo-white.svg)',
                    backgroundPosition: 'center center',
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'contain',
                  }}
                />
                <Typography
                  variant="body1"
                  fontWeight="bold"
                  sx={{
                    margin: theme.spacing(1, 2),
                    position: 'absolute',
                    bottom: 0,
                    textShadow: '0px 0px 12px rgba(0, 0, 0, 0.5)',
                  }}
                >
                  <InlineLink to="#">AirPods Pro 2 Box</InlineLink>
                </Typography>
              </>
            ),
            gridSize: new Vector2(2, 2),
          },
        ].map((itemData) => {
          return (
            <Box
              component="div"
              sx={{
                position: 'relative',
                height: MARQUEE_HEIGHT,
                aspectRatio: `${itemData.gridSize.x} / ${itemData.gridSize.y}`,
                marginRight: 2,
              }}
            >
              {itemData.children}
            </Box>
          );
        })}
      </ProjectsMarquee> */}
      <Container maxWidth="md" sx={{ paddingTop: 4, paddingBottom: 4 }}>
        <TransdimensionalRect id="projects-reference-panel" display="block">
          <Box component="div" sx={{ padding: 2 }}>
            <Typography variant="body1">
              You can see the full list of my projects and their associated
              documentation at{' '}
              <ChipInlineWrapper>
                <TransdimensionalRect
                  id="mainPage_projectsSection_referenceChip"
                  display="inline-block"
                >
                  <TransdimensionalOffset
                    z={theme.depth.chip}
                    display="inline-block"
                  >
                    <Chip>
                      <ThemeInverter>
                        <Breadcrumbs
                          variant="chip"
                          prefixBrandCircle
                          items={[
                            {
                              to: '/reference',
                              text: 'Reference',
                            },
                            {
                              to: '/reference/projects',
                              text: 'Projects',
                            },
                          ]}
                        />
                      </ThemeInverter>
                    </Chip>
                  </TransdimensionalOffset>
                </TransdimensionalRect>
              </ChipInlineWrapper>
              .
            </Typography>
          </Box>
        </TransdimensionalRect>
      </Container>
    </Section>
  );
};

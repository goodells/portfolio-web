import { FC } from 'react';
import { Container } from '@mui/material';
import { PageFooter, PageTitle } from '../..';
import {
  MainPageIntroductionSection,
  MainPageServicesSection,
  MainPageProjectsSection,
  MainPageEducationSection,
  MainPageCertificationsSection,
  MainPageContactSection,
} from '.';

import './MainPage.scss';

export const MainPage: FC = () => {
  return (
    <>
      <PageTitle>Portfolio</PageTitle>

      {/* <ApplicationScrollContainer height="300vh"> */}
      <MainPageIntroductionSection />
      <MainPageServicesSection />
      <MainPageProjectsSection />
      <MainPageEducationSection />
      <MainPageCertificationsSection />
      <MainPageContactSection />
      <Container maxWidth="md">
        <PageFooter />
      </Container>
    </>
  );
};

import { FC } from 'react';
import { Container, Typography } from '@mui/material';
import { Section } from '../../..';

export const MainPageExperienceSection: FC = () => {
  return (
    <Section id="section-experience">
      <Container maxWidth="md">
        <Typography variant="h2">My Experience</Typography>
      </Container>
    </Section>
  );
};

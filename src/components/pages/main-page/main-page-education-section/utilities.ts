export const getEducationItemTitleRectId = (educationItemId: string) =>
  `${educationItemId}-title`;

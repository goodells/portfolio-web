import { FC, PropsWithChildren } from 'react';
import { Box, Stack, Typography, useMediaQuery, useTheme } from '@mui/material';
import {
  SERVICES_DEVELOP_SERVICE_ITEM_SECTIONS,
  SERVICES_DESIGN_SERVICE_ITEM_SECTIONS,
  SERVICES_DEPLOY_SERVICE_ITEM_SECTIONS,
} from '../../../../constants';
import { ServiceItemSectionProps } from '../../../../types';
import { useWindowSize } from '../../../../hooks';
import { roundScalar } from '../../../../utilities';
import {
  Badge,
  Chip,
  ChipList,
  Heading,
  Icon,
  LinearProgressArrow,
  RadialProgressArrow,
  StaticEffectMask,
  TransdimensionalRect,
} from '../../..';
import {
  MainPageServicesGridDevelopThree,
  MainPageServicesGridDesignThree,
  MainPageServicesGridDeployThree,
} from '.';
import { useSelector } from 'react-redux';
import { getOrCreateTransdimensionalRectSelector } from '../../../../selectors';

const ServicesDevelopRadialProgressArrow: FC = () => {
  const servicesGridRect = useSelector(
    getOrCreateTransdimensionalRectSelector('mainPage_servicesGrid_main')
  );
  const servicesGridHeadingRect = useSelector(
    getOrCreateTransdimensionalRectSelector('mainPage_servicesGrid_heading')
  );
  const developHeadingRect = useSelector(
    getOrCreateTransdimensionalRectSelector(
      'mainPage_servicesGrid_develop_heading'
    )
  );

  if (!servicesGridRect || !developHeadingRect) return null;

  const servicesTopY = servicesGridRect.top;
  const servicesHeadingBottomY =
    servicesGridHeadingRect.top + servicesGridHeadingRect.height;
  const sideLeftX = servicesGridRect.left;
  const developHeadingLeftScreenX = developHeadingRect.left;
  const developHeadingScreenY =
    developHeadingRect.top + developHeadingRect.height / 2;

  const fromX = sideLeftX;
  const toX = developHeadingLeftScreenX - 12;
  const width = toX - fromX;
  const height = developHeadingScreenY - servicesHeadingBottomY - 7;

  return (
    <div className="ServicesGrid-progressArrowContainer">
      <RadialProgressArrow
        id="services_develop_arrow"
        showTopBar={false}
        width={width}
        height={height}
        className="ServicesGrid-arrowSideLeft"
        fromScreenY={servicesTopY}
        toScreenY={developHeadingScreenY}
      />
    </div>
  );
};

const ServicesDesignRadialProgressArrow: FC = () => {
  const servicesGridRect = useSelector(
    getOrCreateTransdimensionalRectSelector('mainPage_servicesGrid_main')
  );
  const developHeadingRect = useSelector(
    getOrCreateTransdimensionalRectSelector(
      'mainPage_servicesGrid_develop_heading'
    )
  );
  const designHeadingRect = useSelector(
    getOrCreateTransdimensionalRectSelector(
      'mainPage_servicesGrid_design_heading'
    )
  );

  if (!servicesGridRect || !developHeadingRect || !designHeadingRect)
    return null;

  const servicesTopY = servicesGridRect.top;
  const sideRightX = servicesGridRect.right;
  const developHeadingRightScreenX = developHeadingRect.right;
  const developHeadingScreenY =
    developHeadingRect.top + developHeadingRect.height / 2;
  const designHeadingRightScreenX = designHeadingRect.right;
  const designHeadingScreenY =
    designHeadingRect.top + designHeadingRect.height / 2;

  const fromX = Math.min(developHeadingRightScreenX, designHeadingRightScreenX);
  const width = sideRightX - fromX - 12;
  const height = designHeadingScreenY - developHeadingScreenY;
  const topBarWidth = sideRightX - developHeadingRightScreenX - 12;
  const bottomBarWidth = sideRightX - designHeadingRightScreenX - 12;
  const top = developHeadingScreenY - servicesTopY - 7;

  return (
    <div
      className="ServicesGrid-progressArrowContainer ServicesGrid-progressArrowSideRightContainer"
      style={{
        top: `${top}px`,
      }}
    >
      <RadialProgressArrow
        id="services_design_arrow"
        showTopBar
        topBarWidth={topBarWidth}
        bottomBarWidth={bottomBarWidth}
        width={width}
        height={height}
        className="ServicesGrid-arrowSideRight"
        fromScreenY={developHeadingScreenY}
        toScreenY={designHeadingScreenY}
      />
    </div>
  );
};

const ServicesDeployRadialProgressArrow: FC = () => {
  const servicesGridRect = useSelector(
    getOrCreateTransdimensionalRectSelector('mainPage_servicesGrid_main')
  );
  const designHeadingRect = useSelector(
    getOrCreateTransdimensionalRectSelector(
      'mainPage_servicesGrid_design_heading'
    )
  );
  const deployHeadingRect = useSelector(
    getOrCreateTransdimensionalRectSelector(
      'mainPage_servicesGrid_deploy_heading'
    )
  );

  if (!servicesGridRect || !designHeadingRect || !deployHeadingRect)
    return null;

  const sideLeftX = servicesGridRect.left;
  const servicesTopY = servicesGridRect.top;
  const designHeadingLeftScreenX = designHeadingRect.left;
  const designHeadingScreenY =
    designHeadingRect.top + designHeadingRect.height / 2;
  const deployHeadingLeftScreenX = deployHeadingRect.left;
  const deployHeadingcreenY =
    deployHeadingRect.top + deployHeadingRect.height / 2;

  const fromX = Math.max(designHeadingLeftScreenX, deployHeadingLeftScreenX);
  const width = fromX - sideLeftX - 12;
  const height = deployHeadingcreenY - designHeadingScreenY;
  const topBarWidth = designHeadingLeftScreenX - sideLeftX - 12;
  const bottomBarWidth = deployHeadingLeftScreenX - sideLeftX - 12;
  const top = designHeadingScreenY - servicesTopY - 7;

  return (
    <div
      className="ServicesGrid-progressArrowContainer"
      style={{
        top: `${top}px`,
      }}
    >
      <RadialProgressArrow
        id="services_deploy_arrow"
        showTopBar
        topBarWidth={topBarWidth}
        bottomBarWidth={bottomBarWidth}
        width={width}
        height={height}
        className="ServicesGrid-progressArrow"
        fromScreenY={designHeadingScreenY}
        toScreenY={deployHeadingcreenY}
      />
    </div>
  );
};

const ServicesLinearProgressArrow: FC = () => {
  const windowSize = useWindowSize();
  const screenRightX = windowSize.x;
  const servicesGridRect = useSelector(
    getOrCreateTransdimensionalRectSelector('mainPage_servicesGrid_main')
  );
  const deployHeadingRect = useSelector(
    getOrCreateTransdimensionalRectSelector(
      'mainPage_servicesGrid_deploy_heading'
    )
  );

  if (!servicesGridRect || !deployHeadingRect) return null;

  const servicesTopY = servicesGridRect.top;
  const sideRightX = servicesGridRect.right;

  const deployHeadingRightScreenX = deployHeadingRect.right;
  const deployHeadingcreenY =
    deployHeadingRect.top + deployHeadingRect.height / 2;

  const fromX = deployHeadingRightScreenX;
  const width = screenRightX - fromX - 2 * 12;
  const top = deployHeadingcreenY - servicesTopY - 7 - 12;
  const left = `calc(100% - ${roundScalar(
    sideRightX - deployHeadingRightScreenX - 12
  )}px)`;

  return (
    <Box
      component="div"
      sx={{
        position: 'absolute',
        top,
        left,
      }}
    >
      <LinearProgressArrow
        id="services_linear_arrow"
        width={width}
        className="ServicesGrid-arrowSideRight"
        fromScreenY={deployHeadingcreenY}
        toScreenY={servicesGridRect.top + servicesGridRect.height}
      />
    </Box>
  );
};

const renderServiceItemSections = (
  itemSections: ServiceItemSectionProps[],
  props?: { align?: 'left' | 'center' | 'right' }
) => {
  const theme = useTheme();
  const textColor = theme.palette.getContrastText(
    theme.palette.background.chip
  );

  return (
    <Stack direction="column" spacing={2}>
      {itemSections.map((itemSectionProps) => {
        return (
          <ChipList
            key={itemSectionProps.id}
            {...props}
            label={itemSectionProps.label}
          >
            {itemSectionProps.items.map((itemProps) => {
              return (
                <TransdimensionalRect
                  key={itemProps.id}
                  id={itemProps.id}
                  display="inline-block"
                >
                  <Chip
                    key={itemProps.id}
                    startAdornment={
                      itemProps.iconUrl && (
                        <Icon url={itemProps.iconUrl} color={textColor} />
                      )
                    }
                  >
                    {itemProps.label}
                    {itemProps.badge && (
                      <Badge
                        color={itemProps.badgeColor}
                        background={itemProps.badgeBackground}
                        spacingLeft
                      >
                        {itemProps.badge}
                      </Badge>
                    )}
                  </Chip>
                </TransdimensionalRect>
              );
            })}
          </ChipList>
        );
      })}
    </Stack>
  );
};

const useServicesGridSideWidth = () => {
  const theme = useTheme();

  if (useMediaQuery(theme.breakpoints.down('md'))) {
    return '48px';
  } else {
    return '100px';
  }
};

const ServicesGridContentBody: FC<
  PropsWithChildren<{
    align: 'left' | 'right' | 'center';
  }>
> = (props) => {
  const theme = useTheme();
  const sideWidth = useServicesGridSideWidth();

  return (
    <Box
      component="div"
      className="ServicesGrid-contentSection-body"
      sx={{
        padding: theme.spacing(0, 2),
        [theme.breakpoints.down('sm')]: {
          //   width: `calc(100% + ${sideWidth})`,
          ...(props.align === 'left' && {
            marginRight: sideWidth,
            paddingLeft: 0,
            textAlign: 'left',
          }),
          ...(props.align === 'right' && {
            marginLeft: sideWidth,
            paddingRight: 0,
            textAlign: 'right',
          }),
        },
      }}
    >
      {props.children}
    </Box>
  );
};

export const MainPageServicesGrid: FC = () => {
  const theme = useTheme();
  const sideWidth = useServicesGridSideWidth();
  const isCompactLayout = useMediaQuery(theme.breakpoints.down('md'));
  const headingSx = {
    fontSize: '3.8rem',
    [theme.breakpoints.down('sm')]: {
      fontSize: '3.5rem',
    },
  };

  return (
    <Box
      component="div"
      className="ServicesGrid-container"
      sx={{
        position: 'relative',
        [theme.breakpoints.up('sm')]: {
          // width: `calc(100% - ${theme.spacing(3)} * 2)`,
          width: '100%',
          // margin: theme.spacing(0, 3),
        },
        [theme.breakpoints.down('sm')]: {
          width: `calc(100% - ${theme.spacing(2)} * 2)`,
          // width: '100%',
          margin: theme.spacing(0, 2),
        },
      }}
    >
      {/* <div
        style={{
          position: 'absolute',
          left: '0px',
          top: '352px',
          width: '100%',
          height: 'calc(100% - 352px)',
        }}
      /> */}
      <TransdimensionalRect
        id="mainPage_servicesGrid_main"
        className="ServicesGrid"
        component="div"
        sx={{
          [theme.breakpoints.down('sm')]: {
            '& > .ServicesGrid-side': {
              position: 'absolute',
            },
          },
        }}
      >
        <Box
          component="div"
          className="ServicesGrid-side ServicesGrid-side-left"
          sx={{
            width: sideWidth,
            [theme.breakpoints.down('sm')]: {
              position: 'absolute',
              left: 0,
            },
          }}
        >
          <TransdimensionalRect
            id="mainPage_servicesGrid_heading"
            component="div"
            display="inline-block"
          >
            <Heading variant="h2" id="what-i-do">
              What I Do
            </Heading>
          </TransdimensionalRect>
          <ServicesDevelopRadialProgressArrow />
          <ServicesDeployRadialProgressArrow />
        </Box>
        <div className="ServicesGrid-content">
          <div className="ServicesGrid-contentSection AlignmentContext AlignmentContext-alignCenter">
            <StaticEffectMask>
              <Typography variant="h3" sx={headingSx}>
                <TransdimensionalRect
                  id="mainPage_servicesGrid_develop_heading"
                  component="div"
                  display="inline-block"
                >
                  Develop
                </TransdimensionalRect>
              </Typography>
            </StaticEffectMask>
            <ServicesGridContentBody align="left">
              <br />
              <MainPageServicesGridDevelopThree />
              {renderServiceItemSections(
                SERVICES_DEVELOP_SERVICE_ITEM_SECTIONS,
                {
                  align: isCompactLayout ? 'left' : 'center',
                }
              )}
              <br />
            </ServicesGridContentBody>
          </div>
          <div className="ServicesGrid-contentSection AlignmentContext AlignmentContext-alignCenter">
            <StaticEffectMask>
              <Typography variant="h3" sx={headingSx}>
                <TransdimensionalRect
                  id="mainPage_servicesGrid_design_heading"
                  component="div"
                  display="inline-block"
                >
                  Design
                </TransdimensionalRect>
              </Typography>
            </StaticEffectMask>
            <ServicesGridContentBody align="right">
              <br />
              <MainPageServicesGridDesignThree />
              {renderServiceItemSections(
                SERVICES_DESIGN_SERVICE_ITEM_SECTIONS,
                {
                  align: isCompactLayout ? 'right' : 'center',
                }
              )}
              <br />
            </ServicesGridContentBody>
          </div>
          <div className="ServicesGrid-contentSection AlignmentContext AlignmentContext-alignCenter">
            <StaticEffectMask>
              <Typography variant="h3" sx={headingSx}>
                <TransdimensionalRect
                  id="mainPage_servicesGrid_deploy_heading"
                  component="div"
                  display="inline-block"
                >
                  Deploy
                </TransdimensionalRect>
              </Typography>
            </StaticEffectMask>
            <ServicesGridContentBody align="center">
              <br />
              <MainPageServicesGridDeployThree />
              {renderServiceItemSections(
                SERVICES_DEPLOY_SERVICE_ITEM_SECTIONS,
                {
                  align: 'center',
                }
              )}
              <br />
            </ServicesGridContentBody>
          </div>
        </div>
        <Box
          component="div"
          className="ServicesGrid-side ServicesGrid-side-right"
          sx={{
            width: sideWidth,
            [theme.breakpoints.down('sm')]: {
              position: 'absolute',
              right: 0,
            },
          }}
        >
          <ServicesDesignRadialProgressArrow />
          <ServicesLinearProgressArrow />
        </Box>
      </TransdimensionalRect>
    </Box>
  );
};

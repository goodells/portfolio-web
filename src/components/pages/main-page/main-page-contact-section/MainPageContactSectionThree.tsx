import { FC } from 'react';
import {
  MeetingCreationThree,
  Three,
  ThreeLighting,
  ThreeScrollOffsetContainer,
} from '../../..';

export const MainPageContactSectionThree: FC = () => {
  return (
    <Three
      id="mainPage_contactSection_three"
      inset
      sx={{
        position: 'absolute',
        width: '100%',
        height: '100%',
      }}
    >
      <ThreeLighting />
      <ThreeScrollOffsetContainer>
        <MeetingCreationThree id="" />
      </ThreeScrollOffsetContainer>
    </Three>
  );
};

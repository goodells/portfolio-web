import { FC } from 'react';

export const ProjectAirBlock: FC = () => {
  return (
    <div className="Project-container">
      <div className="ProjectCard">
        <img src="/projects/airblock/logo.png" />
        <p>
          <b>AirBlock</b> turns your AirPlay enabled speakers into a smart white
          noise system.
        </p>
      </div>
    </div>
  );
};

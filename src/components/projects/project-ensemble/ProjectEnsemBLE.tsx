import { FC } from 'react';

export const ProjectEnsemBLE: FC = () => {
  return (
    <div className="Project-container">
      <div className="ProjectCard">
        <img src="/projects/ensemble/logo.png" />
        <p>
          <b>EnsemBLE</b> lets all of your Bluetooth Low Energy devices team up
          for better area coverage.
        </p>
      </div>
    </div>
  );
};

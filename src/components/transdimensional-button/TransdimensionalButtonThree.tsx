import {
  FC,
  useState,
  useEffect,
  useContext,
  createContext,
  PropsWithChildren,
} from 'react';
import { useTheme } from '@mui/material';
import { TransdimensionalInteractionServiceContext } from '../../contexts';
import { ElementState } from '../../types';
import { ThreeTransdimensionalRect } from '..';

const DEFAULT_STATE: ElementState = {
  disabled: false,
  hover: false,
  active: false,
};

export const TransdimensionalButtonThreeContext =
  createContext<ElementState>(DEFAULT_STATE);

export const TransdimensionalButtonThree: FC<
  PropsWithChildren<{
    id: string;
    depth?: number;
  }>
> = ({ id, children, ...props }) => {
  const theme = useTheme();
  const transdimensionalInteractionService = useContext(
    TransdimensionalInteractionServiceContext
  );

  const depth = props.depth || theme.depth.button;
  const [state, setState] = useState<ElementState>(DEFAULT_STATE);

  useEffect(() => {
    transdimensionalInteractionService.onChange(id, setState as any);
  }, [transdimensionalInteractionService, setState]);

  useEffect(() => {
    if (!state) return;
  }, [state]);

  return (
    <ThreeTransdimensionalRect id={id}>
      <object3D position={[0, 0, -depth / 2]} scale={[1, 1, depth]}>
        <TransdimensionalButtonThreeContext.Provider value={state}>
          {children}
        </TransdimensionalButtonThreeContext.Provider>
      </object3D>
    </ThreeTransdimensionalRect>
  );
};

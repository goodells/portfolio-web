import { FC, ReactElement } from 'react';
import { useSearchParams } from 'react-router-dom';
import { useTheme } from '@mui/material';

import {
  ChipThree,
  ThreeBox,
  TransdimensionalButtonThree,
  TransdimensionalButtonThreeBox,
  ThreeTransdimensionalRect,
} from '..';
import { useSearchParamsDate } from './utilities';

export const MeetingCreationThree: FC<{
  id: string;
}> = (_props) => {
  const theme = useTheme();
  const [searchParams, _setSearchParams] = useSearchParams();
  const { date: selectedDate } = useSearchParamsDate(searchParams);
  const calendarDateElements: ReactElement[] = [];

  for (let date = 1; date <= 31; date++) {
    const isSelected = date === selectedDate;

    calendarDateElements.push(
      <TransdimensionalButtonThree key={date} id={date.toString()}>
        <TransdimensionalButtonThreeBox
          colorProfile={isSelected ? 'secondary' : 'default'}
        />
      </TransdimensionalButtonThree>
    );
  }

  return (
    <>
      <ThreeTransdimensionalRect id="meetingCreation_meetingTypeDetails">
        <ThreeBox
          position={[0, 0, -theme.depth.paper / 2]}
          scale={[1, 1, theme.depth.paper]}
        />
      </ThreeTransdimensionalRect>
      <ChipThree
        id="meetingCreation_meetingDurationChip"
        depthOffset="foreground"
      />
      {calendarDateElements}
    </>
  );
};

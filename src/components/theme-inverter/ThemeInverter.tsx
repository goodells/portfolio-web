import { FC, PropsWithChildren } from 'react';
import { ThemeProvider } from '..';
import { useInvertedTheme } from '../../hooks';

export const ThemeInverter: FC<PropsWithChildren<{}>> = (props) => {
  const invertedTheme = useInvertedTheme();

  return <ThemeProvider theme={invertedTheme}>{props.children}</ThemeProvider>;
};

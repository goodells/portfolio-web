import { FC, ReactNode } from 'react';
import { Button, Grid } from '@mui/material';

export interface GridSelectionOption {
  id: string;
  content: ReactNode;
}

export interface GridSelectionProps {
  selectedOptionIds: Set<string>;
  options: GridSelectionOption[];
  onClickOption?: (optionId: string) => void;
}

export const GridSelection: FC<GridSelectionProps> = (props) => {
  return (
    <Grid container spacing={0}>
      {props.options.map((option) => {
        const isSelected = props.selectedOptionIds.has(option.id);

        return (
          <Grid key={option.id} item md={4}>
            <Button
              type="button"
              variant={isSelected ? 'contained' : 'text'}
              fullWidth
              disableElevation
              disableRipple
              sx={{
                padding: 2,
                textAlign: 'center',
              }}
              onClick={() => {
                if (props.onClickOption) props.onClickOption(option.id);
              }}
            >
              {option.content}
            </Button>
          </Grid>
        );
      })}
    </Grid>
  );
};

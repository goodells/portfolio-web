import { FC, ReactNode, useState } from 'react';
import { EasingType } from '../../types';
import { useScreenRangeEffect } from '../../hooks';

export const ScreenRangeEffect: FC<{
  fromScreenY: number;
  toScreenY: number;
  anchorOffset: number;
  easingType?: EasingType;
  children: (props: { ratio: number | null }) => ReactNode;
}> = ({ fromScreenY, toScreenY, anchorOffset, easingType, children }) => {
  if (typeof children !== 'function') {
    throw new TypeError('Children must be specified as a callback');
  }

  const [ratio, setRatio] = useState<number | null>(null);

  const applyAnchorOffset = (screenY: number) => {
    return screenY + (anchorOffset ?? 0);
  };

  useScreenRangeEffect({
    easingType,
    fromScreenY: applyAnchorOffset(fromScreenY),
    toScreenY: applyAnchorOffset(toScreenY),
    onTriggerRatio: setRatio,
  });

  return <>{children({ ratio })}</>;
};

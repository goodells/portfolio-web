import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 5 3 C 3.9069372 3 3 3.9069372 3 5 L 3 19 C 3 20.093063 3.9069372 21 5 21 L 19 21 C 20.093063 21 21 20.093063 21 19 L 21 5 C 21 3.9069372 20.093063 3 19 3 L 5 3 z M 5 5 L 19 5 L 19 19 L 5 19 L 5 5 z M 10 6 L 10 8 L 11.464844 8 L 13.464844 11 L 18 11 L 18 9 L 14.535156 9 L 12.535156 6 L 10 6 z M 15 6 L 15 8 L 18 8 L 18 6 L 15 6 z" />,
  'OptionOutlined'
);

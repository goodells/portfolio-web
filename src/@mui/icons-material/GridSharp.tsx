import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 3 3 L 3 11 L 11 11 L 11 3 L 3 3 z M 13 3 L 13 11 L 21 11 L 21 3 L 13 3 z M 3 13 L 3 21 L 11 21 L 11 13 L 3 13 z M 13 13 L 13 21 L 21 21 L 21 13 L 13 13 z" />,
  'GridSharp'
);

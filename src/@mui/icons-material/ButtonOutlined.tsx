import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 2 7 C 0.9069372 7 0 7.9069372 0 9 L 0 15 C 0 16.093063 0.9069372 17 2 17 L 22 17 C 23.093063 17 24 16.093063 24 15 L 24 9 C 24 7.9069372 23.093063 7 22 7 L 2 7 z M 2 9 L 22 9 L 22 15 L 2 15 L 2 9 z M 7 11 L 7 13 L 9 13 L 9 11 L 7 11 z M 11 11 L 11 13 L 13 13 L 13 11 L 11 11 z M 15 11 L 15 13 L 17 13 L 17 11 L 15 11 z" />,
  'ButtonOutlined'
);

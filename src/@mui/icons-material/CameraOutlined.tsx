import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 3 5 C 1.9069372 5 1 5.9069372 1 7 L 1 17 C 1 18.093063 1.9069372 19 3 19 L 16 19 C 17.093063 19 18 18.093063 18 17 L 18 14.080078 L 23 18.080078 L 23 5.9199219 L 18 9.9199219 L 18 7 C 18 5.9069372 17.093063 5 16 5 L 3 5 z M 3 7 L 16 7 L 16 17 L 3 17 L 3 7 z M 21 10.082031 L 21 13.917969 L 18.601562 12 L 21 10.082031 z" />,
  'CameraOutlined'
);

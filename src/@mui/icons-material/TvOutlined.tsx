import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 3 3 C 1.895 3 1 3.895 1 5 L 1 17 C 1 18.105 1.895 19 3 19 L 8 19 L 8 21 L 16 21 L 16 19 L 21 19 C 22.105 19 23 18.105 23 17 L 23 5 C 23 3.895 22.105 3 21 3 L 3 3 z M 3 5 L 21 5 L 21 17 L 3 17 L 3 5 z" />,
  'TvOutlined'
);

import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 12 1 L 3 5 L 3 11 C 3 18.83 9.439 22.486 12 23 C 14.561 22.486 21 18.83 21 11 L 21 5 L 12 1 z M 12 3.1894531 L 19 6.3007812 L 19 11 C 19 17.134 14.215 20.2545 12 20.9375 C 9.785 20.2545 5 17.134 5 11 L 5 6.3007812 L 12 3.1894531 z M 12 7 L 8 13 L 11 13 L 11 17 L 15 11 L 12 11 L 12 7 z" />,
  'SecurityEnergyOutlined'
);

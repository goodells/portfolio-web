import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 6 2 C 4.9069372 2 4 2.9069372 4 4 L 4 20 C 4 21.093063 4.9069372 22 6 22 L 18 22 C 19.093063 22 20 21.093063 20 20 L 20 4 C 20 2.9069372 19.093063 2 18 2 L 6 2 z M 6 4 L 11 4 L 11 9 L 6 9 L 6 4 z M 13 4 L 18 4 L 18 9 L 13 9 L 13 4 z M 6 11 L 11 11 L 11 20 L 6 20 L 6 11 z M 13 11 L 18 11 L 18 20 L 13 20 L 13 11 z" />,
  'ClosedWindowOutlined'
);

import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 4 2 C 2.895 2 2 2.895 2 4 L 2 20 C 2 21.105 2.895 22 4 22 L 10 22 L 10 20 L 4 20 L 4 16 L 10 16 L 10 14 L 4 14 L 4 4 L 12 4 L 12 11 L 14 11 L 14 4 C 14 2.895 13.105 2 12 2 L 4 2 z M 6 6 L 6 8 L 10 8 L 10 6 L 6 6 z M 6 10 L 6 12 L 10 12 L 10 10 L 6 10 z M 12 13 L 12 22 L 22 22 L 22 14 L 21 14 L 17.523438 14 L 16.183594 13 L 12 13 z M 14 15 L 15.519531 15 L 16.859375 16 L 20 16 L 20 20 L 14 20 L 14 15 z M 8 17 A 1 1 0 0 0 7 18 A 1 1 0 0 0 8 19 A 1 1 0 0 0 9 18 A 1 1 0 0 0 8 17 z" />,
  'FtpServerOutlined'
);

import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M2 4H22V11H2zM2 13H22V20H2z" />,
  'RowsSharp'
);

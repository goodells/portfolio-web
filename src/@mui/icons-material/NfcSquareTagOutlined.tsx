import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 5 3 C 3.9069372 3 3 3.9069372 3 5 L 3 19 C 3 20.093063 3.9069372 21 5 21 L 19 21 C 20.093063 21 21 20.093063 21 19 L 21 5 C 21 3.9069372 20.093063 3 19 3 L 5 3 z M 5 5 L 19 5 L 19 19 L 5 19 L 5 5 z M 7 7 L 7 13.414062 L 10.585938 17 L 17 17 L 17 16 L 17 7 L 7 7 z M 9 9 L 15 9 L 15 15 L 11.414062 15 L 9 12.585938 L 9 9 z M 7 16 A 1 1 0 0 0 6 17 A 1 1 0 0 0 7 18 A 1 1 0 0 0 8 17 A 1 1 0 0 0 7 16 z" />,
  'NfcSquareTagOutlined'
);

import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" version="1.1">
    <defs>
      <mask id="default-mask">
        <circle cx="12" cy="12" r="10" fill="white" />
      </mask>
      <circle cx="12" cy="12" r="10" fill="url(#default-gradient)" />
      <linearGradient id="linear-gradient" gradientTransform="rotate(45)">
        <stop offset="0%" stop-color="rgb(255, 64, 0)" />
        <stop offset="25%" stop-color="rgb(255, 64, 0)" />
        <stop offset="45%" stop-color="rgb(255, 148, 32)" />
        <stop offset="75%" stop-color="rgb(238, 238, 64)" />
        <stop offset="100%" stop-color="rgb(238, 238, 64)" />
      </linearGradient>
    </defs>
    <g mask="url(#default-mask)">
      <circle cx="12" cy="12" r="10" fill="url(#linear-gradient)" />
      <circle
        cx="10.5"
        cy="10.5"
        r="8.75"
        fill="rgb(255, 64, 0)"
        filter="blur(3.2px)"
        opacity="0.625"
      />
    </g>
  </svg>,
  'SamuelGoodellColor'
);

import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 5 3 C 3.9069372 3 3 3.9069372 3 5 L 3 16 C 3 17.093063 3.9069372 18 5 18 L 11 18 L 11 20 L 2 20 L 2 22 L 22 22 L 22 20 L 13 20 L 13 18 L 19 18 C 20.093063 18 21 17.093063 21 16 L 21 5 C 21 3.9069372 20.093063 3 19 3 L 5 3 z M 5 5 L 19 5 L 19 9 L 5 9 L 5 5 z M 7 6 A 1 1 0 0 0 6 7 A 1 1 0 0 0 7 8 A 1 1 0 0 0 8 7 A 1 1 0 0 0 7 6 z M 10 6 A 1 1 0 0 0 9 7 A 1 1 0 0 0 10 8 A 1 1 0 0 0 11 7 A 1 1 0 0 0 10 6 z M 13 6 L 13 8 L 17 8 L 17 6 L 13 6 z M 5 11 L 19 11 L 19 16 L 5 16 L 5 11 z" />,
  'RouterOutlined'
);

import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 2 3 L 2 5 L 5 5 L 5 6 L 2 6 L 2 8 L 5 8 L 5 9 L 2 9 L 2 11 L 5 11 L 5 19.117188 C 5 20.155187 5.8334219 21 6.8574219 21 L 17.142578 21 C 18.166578 21 19 20.155188 19 19.117188 L 19 11 L 22 11 L 22 9 L 19 9 L 19 8 L 22 8 L 22 6 L 19 6 L 19 5 L 22 5 L 22 3 L 2 3 z M 7 5 L 17 5 L 17 6 L 7 6 L 7 5 z M 7 8 L 17 8 L 17 9 L 7 9 L 7 8 z M 7 11 L 17 11 L 17 19 L 7 19 L 7 11 z M 21 13 A 1 1 0 0 0 20 14 A 1 1 0 0 0 21 15 A 1 1 0 0 0 22 14 A 1 1 0 0 0 21 13 z M 21 16 A 1 1 0 0 0 20 17 A 1 1 0 0 0 21 18 A 1 1 0 0 0 22 17 A 1 1 0 0 0 21 16 z M 21 19 A 1 1 0 0 0 20 20 A 1 1 0 0 0 21 21 A 1 1 0 0 0 22 20 A 1 1 0 0 0 21 19 z" />,
  'JalousieOutlined'
);

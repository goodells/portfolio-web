import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 5 3 C 3.9069372 3 3 3.9069372 3 5 L 3 19 C 3 20.093063 3.9069372 21 5 21 L 19 21 C 20.093063 21 21 20.093063 21 19 L 21 5 C 21 3.9069372 20.093063 3 19 3 L 5 3 z M 5 5 L 19 5 L 19 19 L 5 19 L 5 5 z M 9 6 L 6 9 L 9 12 L 9 10 L 11 10 L 11 8 L 9 8 L 9 6 z M 9 12 L 6 15 L 9 18 L 9 16 L 11 16 L 11 14 L 9 14 L 9 12 z M 15 6 L 15 8 L 13 8 L 13 10 L 15 10 L 15 12 L 17 10 L 18 9 L 17 8 L 15 6 z M 15 12 L 15 14 L 13 14 L 13 16 L 15 16 L 15 18 L 18 15 L 15 12 z" />,
  'SwitchOutlined'
);

import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 6 2 C 4.895 2 4 2.895 4 4 L 4 20 C 4 21.105 4.895 22 6 22 L 14.005859 22 C 13.674859 21.406 13.412328 20.737 13.236328 20 L 6 20 L 6 16 L 13 16 L 13 14 L 6 14 L 6 10 L 18 10 L 18 11.484375 L 20 10.599609 L 20 4 C 20 2.895 19.105 2 18 2 L 6 2 z M 6 4 L 18 4 L 18 8 L 6 8 L 6 4 z M 19.5 13 L 15 15 L 15 18 C 15 21.915 18.22 23.743 19.5 24 C 20.78 23.743 24 21.915 24 18 L 24 15 L 19.5 13 z" />,
  'DataProtectionOutlined'
);

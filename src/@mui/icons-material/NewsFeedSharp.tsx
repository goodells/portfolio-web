import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 3 3 L 3 7 L 21 7 L 21 3 L 3 3 z M 3 10 L 3 14 L 21 14 L 21 10 L 3 10 z M 3 17 L 3 21 L 21 21 L 21 17 L 3 17 z" />,
  'NewsFeedSharp'
);

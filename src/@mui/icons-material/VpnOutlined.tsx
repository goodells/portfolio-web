import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 4 2 C 2.895 2 2 2.895 2 4 L 2 16 C 2 17.105 2.895 18 4 18 L 10 18 L 10 20 L 2 20 L 2 22 L 22 22 L 22 20 L 14 20 L 14 18 L 20 18 C 21.105 18 22 17.105 22 16 L 22 4 C 22 2.895 21.105 2 20 2 L 4 2 z M 4 4 L 20 4 L 20 16 L 4 16 L 4 4 z M 10 6 L 6 10 L 10 14 L 10 11 L 14 11 L 14 14 L 18 10 L 14 6 L 14 9 L 10 9 L 10 6 z" />,
  'VpnOutlined'
);

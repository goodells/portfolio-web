import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 12 2.0996094 L 1 12 L 4 12 L 4 21 L 13.548828 21 C 13.314828 20.392 13.140594 19.729 13.058594 19 L 6 19 L 6 10.189453 L 12 4.7910156 L 18.925781 11 L 21.888672 11 L 12 2.0996094 z M 19.5 13 L 15 15 L 15 18 C 15 21.915 18.22 23.743 19.5 24 C 20.78 23.743 24 21.915 24 18 L 24 15 L 19.5 13 z" />,
  'SmartHomeShieldOutlined'
);

import { createSvgIcon } from '@mui/material';

export default createSvgIcon(
  <path d="M 12 2.5 L 2 6.5 L 2 9 L 3 9 L 3 19 L 2 19 L 2 21 L 22 21 L 22 19 L 21 19 L 21 9 L 22 9 L 22 6.5 L 12 2.5 z M 12 4.6542969 L 17.867188 7 L 6.1328125 7 L 12 4.6542969 z M 5 9 L 7 9 L 7 19 L 5 19 L 5 9 z M 9 9 L 11 9 L 11 19 L 9 19 L 9 9 z M 13 9 L 15 9 L 15 19 L 13 19 L 13 9 z M 17 9 L 19 9 L 19 19 L 17 19 L 17 9 z" />,
  'UniversityOutlined'
);

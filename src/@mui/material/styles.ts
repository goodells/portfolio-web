import { PaletteColor, PaletteColorOptions } from '@mui/material';

declare module '@mui/material/styles' {
  interface Theme {
    depth: Depth;
  }

  interface ThemeOptions {
    depth?: Depth;
  }

  export interface Depth {
    paper: number;
    card: number;
    chip: number;
    button: number;
    input: number;
  }

  interface Palette {
    brandRed: PaletteColor;
    brandOrange: PaletteColor;
    brandYellow: PaletteColor;
    intensityOffset: number;
  }

  interface PaletteOptions {
    brandRed?: PaletteColorOptions;
    brandOrange?: PaletteColorOptions;
    brandYellow?: PaletteColorOptions;
    intensityOffset?: number;
  }

  interface TypeBackground {
    clear: string;
    chip: string;
    object: string;
  }
}

declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    code: true;
  }
}

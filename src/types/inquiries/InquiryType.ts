export enum InquiryType {
  FullTimePosition = 'full_time_position',
  ProjectWork = 'project_work',
  Networking = 'networking',
}

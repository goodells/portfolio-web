import { ReactNode } from 'react';

export interface NavigationItem {
  id: string;
  title: ReactNode;
  icon?: ReactNode;
  path?: string;
  url?: string;
  openInNew?: boolean;
  targetSelector?: string;
  children?: NavigationItem[];
}

import { NavigationItem } from './NavigationItem';

export interface NavigationContext {
  rootPath?: string;
  items: NavigationItem[];
}

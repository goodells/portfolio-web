import { Moment } from 'moment';

export interface DateOption {
  month: number;
  year: number;
  date: Moment;
  isToday: boolean;
  isWeekday: boolean;
  isPadding: boolean;
  hasAvailability: boolean;
  isAvailabilityKnown: boolean;
  isPast: boolean;
  key: string;
}

export interface DocumentSize {
  width: string;
  height: string;
  aspectRatio: string;
}

import { HomeAccessoryCategory } from '.';

export interface HomeAccessory {
  id: string;
  name: string;
  model: string;
  manufacturer: string;
  serial: string;
  firmware: string;
  category: HomeAccessoryCategory;
  pairingCode: string | null;
  setupId: string | null;
  setupPayload: string | null;
  homeRoomId: string | null;
}

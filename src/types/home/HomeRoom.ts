export interface HomeRoom {
  id: string;
  homeId: string;
  name: string;
}

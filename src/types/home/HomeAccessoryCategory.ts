import { Category } from 'homekit-code/lib/types';

export type HomeAccessoryCategory =
  | Category
  | 'homePod'
  | 'homePodMini'
  | 'slidingDoor'
  | 'contactSensor'
  | 'motionSensor'
  | 'occupancySensor';

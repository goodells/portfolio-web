export type ProjectStatus =
  | 'complete'
  | 'active-development'
  | 'experimental'
  | 'maintenance'
  | 'hiatus';

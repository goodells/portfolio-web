import { PresentationChip, PresentationPage } from '..';
import { ProjectStatus } from '.';

export interface Project {
  id: string;
  name: string;
  description: string;
  status: ProjectStatus;
  chip: PresentationChip;
  page: PresentationPage;
}

export interface Progress {
  active: boolean;
  setActive: (active: boolean) => void;
}

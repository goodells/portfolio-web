import { CalendlyProxyClient } from '../../clients';
import { FrameloopMode } from '..';

export interface Environment {
  isDevelopment: boolean;
  isProduction: boolean;
  calendlyProxyUrl: string;
  calendlyProxyClient: CalendlyProxyClient;
  frameloopMode: FrameloopMode;
  localeTimeZoneName: string;
}

import { Color } from 'three';

export type PointColorCoercible =
  | Color
  | [number, number, number]
  | string
  | number;

export type ThreeModelTransformationParameters =
  | string
  | {
      id: string;
      transformationArguments?: any[];
    };

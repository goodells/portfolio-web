// Types
import { ThreeModelTransformationParameters } from '.';

export type ThreeModelTransformationPipelineParameters =
  ThreeModelTransformationParameters[];

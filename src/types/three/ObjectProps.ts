import { QuaternionCoercible, Vector3Coercible } from '.';

export interface ObjectProps {
  position?: Vector3Coercible;
  rotation?: QuaternionCoercible;
  scale?: Vector3Coercible;
}

// THREE.js
import { Color } from 'three';

export interface RectCornerColors {
  topRight: Color;
  topLeft: Color;
  bottomRight: Color;
  bottomLeft: Color;
}

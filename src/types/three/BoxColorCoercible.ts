import { Color } from 'three';
import { BoxCornerColors, RectCornerColors } from '.';

export type BoxColorCoercible =
  | Color
  | [number, number, number]
  | string
  | RectCornerColors
  | [RectCornerColors, RectCornerColors]
  | BoxCornerColors;

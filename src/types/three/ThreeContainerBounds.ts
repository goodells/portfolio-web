export interface ThreeContainerBounds {
  left: number;
  right: number;
}

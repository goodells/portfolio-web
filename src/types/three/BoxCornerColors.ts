import { Color } from 'three';

export interface BoxCornerColors {
  frontTopRight: Color;
  frontTopLeft: Color;
  frontBottomRight: Color;
  frontBottomLeft: Color;
  backTopRight: Color;
  backTopLeft: Color;
  backBottomRight: Color;
  backBottomLeft: Color;
}

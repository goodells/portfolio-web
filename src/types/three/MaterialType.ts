export enum MaterialType {
  Basic = 'basic',
  Standard = 'standard',
  Phong = 'phong',
  Lambert = 'lambert',
}

// THREE.js
import { Object3D } from 'three';

export type ThreeModelTransformation = (
  modelObject: Object3D,
  ...transformationArguments: any[]
) => Promise<void> | void;

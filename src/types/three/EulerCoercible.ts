import { Euler } from 'three';

export type EulerCoercible = Euler | [number, number, number];

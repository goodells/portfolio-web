import { Vector3 } from 'three';

export type Vector3Coercible = Vector3 | [number, number, number] | number;

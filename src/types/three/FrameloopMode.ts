export type FrameloopMode = 'always' | 'demand' | 'never';

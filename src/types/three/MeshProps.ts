import { Material } from 'three';
import { BoxColorCoercible } from './BoxColorCoercible';

export interface MeshProps {
  material?: Material | null;
  vertexColors?: boolean;
  color?: BoxColorCoercible;
  castShadow?: boolean;
  receiveShadow?: boolean;
}

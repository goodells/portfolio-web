import { ReactElement, ReactNode } from 'react';
import { PresentationPage } from '.';

export interface PresentationTab {
  icon?: ReactElement<any>;
  iconPosition?: 'bottom' | 'end' | 'start' | 'top';
  label: ReactNode;
  page?: PresentationPage;
  href?: string;
}

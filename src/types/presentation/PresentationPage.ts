import { PresentationTab } from '.';

export interface PresentationPage {
  tabs?: PresentationTab[];
  markdown?: string;
}

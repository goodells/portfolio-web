import { ReactNode } from 'react';

export interface PresentationChip {
  background: string;
  color?: string;
  content: ReactNode;
}

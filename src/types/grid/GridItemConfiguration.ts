export interface GridItemConfiguration {
  xs?: number;
  sm?: number;
  md?: number;
  lg?: number;
  xl?: number;
}

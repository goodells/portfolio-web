import { Breakpoint } from '@mui/material';

export type BreakpointsMap = Map<Breakpoint, boolean>;

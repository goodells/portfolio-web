import { ReactNode } from 'react';
import { ServiceItemProps } from '.';

export interface ServiceItemSectionProps {
  id: string;
  label: ReactNode;
  items: ServiceItemProps[];
}

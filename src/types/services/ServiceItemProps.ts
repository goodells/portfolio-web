import { ReactNode } from 'react';

export interface ServiceItemProps {
  id: string;
  iconUrl?: string;
  label: ReactNode;
  badge?: ReactNode;
  badgeBackground?: string;
  badgeColor?: string;
}

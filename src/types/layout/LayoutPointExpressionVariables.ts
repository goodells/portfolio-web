export interface LayoutPointExpressionVariables {
  [variable: string]: number;
}

export interface LayoutAnchor {
  id: string;
  x: number;
  y: number;
}

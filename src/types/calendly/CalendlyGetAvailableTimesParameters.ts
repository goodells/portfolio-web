export interface CalendlyGetAvailableTimesParameters {
  event_type: string;
  start_time: string; // ISO 8601 UTC
  end_time: string; // ISO 8601 UTC
}

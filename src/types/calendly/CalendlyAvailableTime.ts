export interface CalendlyAvailableTime {
  status: 'available' | 'unavailable';
  invitees_remaining: number;
  start_time: string; // ISO 8601 UTC
  scheduling_url: string;
}

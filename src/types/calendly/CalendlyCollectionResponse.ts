import { CalendlyPagination } from '.';

export interface CalendlyCollectionResponse<T> {
  collection: T[];
  pagination?: CalendlyPagination;
}

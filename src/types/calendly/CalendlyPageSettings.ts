export type CalendlyPageSettings = Partial<{
  hideLandingPageDetails: boolean;
  hideEventTypeDetails: boolean;
  hideGdprBanner: boolean;
  backgroundColor: string;
  textColor: string;
  primaryColor: string;
}>;

import { ElementState } from '.';

export type ElementStateUpdate = Partial<ElementState>;

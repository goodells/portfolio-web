export interface TransdimensionalRectOptions {
  id: string;
  offsetTop?: number;
  offsetLeft?: number;
  log?: boolean;
}

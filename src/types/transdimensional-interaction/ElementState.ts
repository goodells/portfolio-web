export interface ElementState {
  disabled: boolean;
  hover: boolean;
  active: boolean;
}

export interface TransdimensionalOffsetOptions {
  z: number;
  containerRect?: DOMRect;
  handleTransform?: (transform: string) => void;
}

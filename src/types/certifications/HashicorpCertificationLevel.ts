export enum HashicorpCertificationLevel {
  Associate = 'associate',
  Professional = 'professional',
}

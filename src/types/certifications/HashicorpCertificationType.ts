export enum HashicorpCertificationType {
  Terraform = 'terraform',
  Vault = 'vault',
  Consul = 'consul',
}

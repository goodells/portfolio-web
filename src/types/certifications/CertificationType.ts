export enum CertificationType {
  AWS = 'aws',
  Hashicorp = 'hashicorp',
}

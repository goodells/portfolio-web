export enum AWSCertificationLevel {
  Foundational = 'foundational',
  Associate = 'associate',
  Professional = 'professional',
  Specialty = 'specialty',
}

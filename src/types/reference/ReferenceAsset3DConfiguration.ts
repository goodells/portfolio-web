export interface ReferenceAsset3DConfiguration {
  id: string;
  name: string;
  description?: string;
}

import { ReactNode } from 'react';

export interface ReferenceIndexItemConfiguration {
  id: string;
  name: ReactNode;
  description: ReactNode;
  url: string;
}

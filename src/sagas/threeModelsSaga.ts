import { takeEvery, call, put } from 'redux-saga/effects';
import { Object3D } from 'three';
import { GLTF } from 'three-stdlib';

import { ThreeModelsService } from '../services';
import * as threeModelsActions from '../actions/threeModelsActions';

function* loadGltfModel(
  action: ReturnType<typeof threeModelsActions.requestThreeModel>
) {
  const { id, url, transform } = action.payload;
  let model = new Object3D();

  switch (true) {
    case url.endsWith('.glb'):
      const modelGltf: GLTF = yield call(ThreeModelsService.fetchGltf, url);

      model = modelGltf.scene;

      break;
    case url.endsWith('.json'):
      model = yield call(ThreeModelsService.fetchJson, url);
  }

  yield call(
    ThreeModelsService.applyTransformationPipeline as any,
    model,
    transform
  );

  yield put(threeModelsActions.setThreeModel({ id, model }));
}

export function* threeModelsSaga() {
  yield takeEvery(threeModelsActions.requestThreeModel.type, loadGltfModel);
}

import {
  Color,
  LineBasicMaterial,
  LineSegments,
  Mesh,
  MeshStandardMaterial,
  Object3D,
  Vector3,
} from 'three';
import { GLTF, GLTFLoader } from 'three-stdlib';

import * as replicadThreeHelper from 'replicad-threejs-helper';

import {
  ThreeModelTransformation,
  ThreeModelTransformationParameters,
  ThreeModelTransformationPipelineParameters,
} from '../types';

const HALF_SCALE = new Vector3(1, 1, 1).divideScalar(2);

export class ThreeModelsService {
  private static transformations = new Map<
    string /* transformation ID */,
    ThreeModelTransformation
  >();

  public static defineTransformation(
    transformationId: string,
    transformation: ThreeModelTransformation
  ) {
    this.transformations.set(transformationId, transformation);
  }

  public static fetchGltf(url: string): Promise<GLTF> {
    return new Promise<GLTF>((resolve, reject) => {
      const loader = new GLTFLoader();
      loader.load(
        url,
        resolve,
        (xhr: ProgressEvent) => {
          if (!xhr.total) return;

          console.log(
            `Progress loading '${url}': ${xhr.loaded} / ${
              xhr.total
            } (${Math.round((100 * xhr.loaded) / xhr.total)}%)`
          );
        },
        (error: ErrorEvent) => {
          console.error(`Error occurred while loading model '${url}':`);
          console.error(error);

          reject(error);
        }
      );
    });
  }

  public static async fetchJson(url: string): Promise<Object3D> {
    const object = new Object3D();

    try {
      const response = await fetch(url);
      const json = (await response.json()) as {
        objects: Array<{
          name: string;
          mesh: {
            faces: {
              triangles: number[];
              vertices: number[];
              normals: number[];
              faceGroups: { start: number; count: number; faceId: number }[];
            };
            edges: {
              lines: number[];
              edgeGroups?: { start: number; count: number; edgeId: number }[];
            };
          };
          material: {
            type: 'standard' | 'basic';
            color: number | string;
            opacity: number;
            metalness: number;
            roughness: number;
          };
        }>;
      };

      (json.objects ?? []).forEach((objectData) => {
        if (!objectData.mesh) return;

        const [facesGeometry] = replicadThreeHelper.syncGeometries(
          [objectData.mesh],
          []
        );
        const color = new Color(objectData.material.color);
        const meshMaterial = new MeshStandardMaterial({
          color,
        });
        const mesh = new Mesh(facesGeometry.faces, meshMaterial);
        mesh.scale.copy(HALF_SCALE);

        object.add(mesh);

        const lineMaterial = new LineBasicMaterial({
          color: color.multiplyScalar(1 / 2),
          depthWrite: false,
          polygonOffset: true,
          polygonOffsetFactor: 1,
          polygonOffsetUnits: 1,
        });
        const lines = new LineSegments(facesGeometry.lines, lineMaterial);
        lines.scale.copy(HALF_SCALE);

        object.add(lines);
      });
    } catch (error) {
      console.error('Error loading model from JSON', url, error);
    } finally {
      return object;
    }
  }

  public static async applyTransformationPipeline(
    model: Object3D,
    pipelineParameters: ThreeModelTransformationPipelineParameters
  ): Promise<void> {
    for (let index = 0; index < pipelineParameters.length; index++) {
      const parameters = pipelineParameters[index];

      await ThreeModelsService.applyTransformation(model, parameters);
    }
  }

  public static async applyTransformation(
    model: Object3D,
    parameters: ThreeModelTransformationParameters
  ): Promise<void> {
    const { id, transformationArguments } =
      typeof parameters === 'string'
        ? { id: parameters, transformationArguments: [] }
        : parameters;
    const transformation = ThreeModelsService.transformations.get(id);

    if (!transformation) {
      console.error(`Error due to unknown transformation with ID '${id}'.`);

      return Promise.reject();
    }

    try {
      await transformation.apply(
        null,
        [model].concat(transformationArguments as any) as [Object3D, ...any]
      );
    } catch (error) {
      console.error(
        `Error occurred while invoking transformation with ID '${id}':`
      );
      console.error(error);

      return Promise.reject(error);
    }
  }
}
